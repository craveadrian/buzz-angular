export const environment = {
  production: true,
  apiUrl: 'https://carenowclt.com/api',
  adminUrl: "https://carenowclt.com/",
  frontUrl: "https://carenowclt.com/",
  env: 'production'
};
