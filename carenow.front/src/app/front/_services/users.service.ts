import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";
import {ResponseOk} from "../../@core/_interfaces/general.interface";
import {UserSubscribeInterface} from "../_interfaces/champion.interfaces";

const headers = new HttpHeaders({
  'Content-Type': 'application/json'
});

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private http: HttpClient
  ) { }


  public subscribe (
    data: UserSubscribeInterface
  ): Observable<ResponseOk> {
    return this.http.post<ResponseOk>(environment.apiUrl + '/user/subscribe', data,
      {headers}
    );
  }

  public unsubscribe (
    email: string
  ): Observable<ResponseOk> {
    return this.http.post<ResponseOk>(environment.apiUrl + '/user/unsubscribe', {email},
      {headers}
    );
  }
}
