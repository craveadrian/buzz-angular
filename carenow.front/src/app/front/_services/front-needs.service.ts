import { Injectable } from '@angular/core';
import {Observable, of} from "rxjs";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import * as moment from 'moment';

import {CustomHttpParamEncoder} from "../../@core/decoder/custom.http.param.decoder";
import {environment} from "../../../environments/environment";
import {ResponseCounter, ResponseDropdown} from "../../@core/_interfaces/general.interface";
import {
  NeedClaimInterface,
  NeedsSearchInterfaces,
  NeedsStatsInterface,
  ResponseUserNeed,
  ResponseUserNeeds,
  ResponseUserNeedsStats
} from "../_interfaces/needs.interfaces";

const headers = new HttpHeaders({
  'Content-Type': 'application/json'
});

@Injectable({
  providedIn: 'root'
})
export class FrontNeedsService {
  needsStats: NeedsStatsInterface = {
    "listed": 0,
    "met": 0
  };

  ageRange$: Observable<{id: string, name: string}[]> = of([
    {id: 'baby', name: 'Baby (0-12 mos)'},
    {id: 'toddler', name: 'Toddler (1-3 yrs)'},
    {id: 'preschool', name: 'Preschool (3-5 yrs)'},
    {id: 'gradeschool', name: 'Gradeschool (5-12 yrs)'},
    {id: 'teen', name: 'Teen (12-18 yrs)'},
    {id: 'adult', name: 'Adult (18+ yrs)'}
  ])

  price$: Observable<{id: {min: number | null, max: number | null}, name: string}[]> = of([
    {id: {min: null, max: 49}, name: '$ (0-49)'},
    {id: {min: 40, max: 99}, name: '$$ (50-99)'},
    {id: {min: 100, max: 199}, name: '$$$ (100-199)'},
    {id: {min: 200, max: null}, name: '$$$$ (200+)'}
  ])

  gender$: Observable<{id: string, name: string}[]> = of([
    {id: 'male', name: 'Male'},
    {id: 'female', name: 'Female'}
  ])

  currentTime = moment().utc().startOf('day').unix();
  threeDaysTime = moment().utc().startOf('day').add(3,'d').unix();
  sevenDaysTime = moment().utc().startOf('day').add(7,'d').unix();
  // threeDaysAgoTime = moment().utc().startOf('day').subtract(3,'d').unix();

  expiresAt$: Observable<{id: {min: number | null, max: number | null}, name: string}[]> = of([
    {id: {min: null, max: this.currentTime}, name: 'Overdue'},
    {id: {min: this.currentTime, max: this.threeDaysTime}, name: '< 3 days left'},
    {id: {min: this.currentTime, max: this.sevenDaysTime}, name: '< 1 week left'},
    {id: {min: this.sevenDaysTime, max: null}, name: '> 1 week left'}
  ])

  constructor(
    private http: HttpClient,
  ) {
  }

  public getNeed(
    id: number
  ): Observable<ResponseUserNeed> {

    let params = new HttpParams({encoder: new CustomHttpParamEncoder()});
    params = params.append('id', id);

    return this.http.get<ResponseUserNeed>(environment.apiUrl + '/user/needs/getObject',
      {params: params})
      ;
  }

  public getNeeds(
    options: NeedsSearchInterfaces
  ): Observable<ResponseUserNeeds> {

    let params = this.searchFilters(options);

    if (options.order) {
      params = params.append('order', options.order);
    }

    if (options.limit) {
      params = params.append('limit', options.limit);
    }

    if (options.skip) {
      params = params.append('skip', options.skip);
    }

    return this.http.get<ResponseUserNeeds>(environment.apiUrl + '/user/needs/getObjects',
      {params: params})
      ;
  }

  public getFeaturedNeeds(): Observable<ResponseUserNeeds>
  {
    return this.http.get<ResponseUserNeeds>(environment.apiUrl + '/user/needs/getFeaturedNeeds');
  }

  public getNeedsCounter(
    options: NeedsSearchInterfaces
  ): Observable<ResponseCounter> {

    const params = this.searchFilters(options);

    return this.http.get<ResponseCounter>(environment.apiUrl + '/user/needs/getObjectsCounter',
      {params: params}
    );
  }

  public getCounties(): Observable<ResponseDropdown>
  {
    return this.http.get<ResponseDropdown>(environment.apiUrl + '/user/needs/getCounties',
      {headers:headers}
    );
  }

  public getNeedsStats(): Observable<ResponseUserNeedsStats> {
    return this.http.get<ResponseUserNeedsStats>(environment.apiUrl + '/user/needs/getStats');
  }

  searchFilters(
    options: NeedsSearchInterfaces
  ) {
    let params = new HttpParams({encoder: new CustomHttpParamEncoder()});

    if (options.keyword) {
      params = params.append('keyword', options.keyword);
    }
    if (options.ageRange && options.ageRange.length > 0) {
      for (let age of options.ageRange) {
        params = params.append('ageRange', age);
      }
    }

    if (options.county) {
      params = params.append('county', options.county);
    }

    if (options.price) {
      params = params.append('price', JSON.stringify(options.price));
    }

    if (options.expiresAt) {
      params = params.append('expiresAt', JSON.stringify(options.expiresAt));
    }

    if (options.gender && options.gender.length > 0) {
      for (let gender of options.gender) {
        params = params.append('gender', gender);
      }
    }


    return params;
  }

  public claimNeed (
    claimNeed: NeedClaimInterface
  ): Observable<ResponseUserNeed> {
    return this.http.post<ResponseUserNeed>(environment.apiUrl + '/user/needs/claim', claimNeed,
      {headers}
    );
  }

  transform(
    unixTime: number
  ): number {

    const a = new Date();
    a.setTime(unixTime);

    const currentDate = new Date();
    const b = new Date();
    b.setFullYear(currentDate.getUTCFullYear());
    b.setMonth(currentDate.getUTCMonth());
    b.setDate(currentDate.getUTCDate());

    return FrontNeedsService.dateDiffInDays(a, b);
  }

  private static dateDiffInDays(
    a: Date,
    b: Date
  ): number {
    const _MS_PER_DAY = 1000 * 60 * 60 * 24;
    const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
  }


}
