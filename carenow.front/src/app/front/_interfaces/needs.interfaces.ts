import {ErrorInterface} from "../../@core/_interfaces/general.interface";
import {ChampionInterfaces} from "./champion.interfaces";


export enum NeedTypes {
  need = "need"
}

export enum NeedStatus {
  draft = "draft",
  listed = "listed",
  claimed = "claimed",
  purchased = "purchased",
  delivered = "delivered",
  pickedUp = "pickedUp"
}

export enum Genders {
  male = "male",
  female = "female",
}

export enum AgeRanges {
  baby = "baby",
  toddler = "toddler",
  preschool = "preschool",
  gradeschool = "gradeschool",
  teen = "teen",
  adult = "adult",
}

export interface NeedClaimInterface {
  "id": number,
  "firstName": string,
  "lastName": string,
  "email": string,
  "phoneNumber": string
}

export interface NeedsSearchInterfaces {
  county?: string,
  price?: {min: number, max: number},
  gender?: Genders[],
  ageRange?: AgeRanges[],
  keyword?: string,
  expiresAt?: {min: number, max: number},
  order?: string,
  skip: number,
  limit: number
}
//
// export interface NeedBulkUpdateInterface {
//   ids: number[],
//   familyName: string;
// }

export interface UserNeedInterface {
  id?: number;
  createdAt?: number;
  updatedAt?: number;
  storageFolder?: string;
  type?: NeedTypes;
  title: string;
  description: string;
  expiresAt?: number;
  price: number;
  imageURL?: string;
  campaignURL?: string;
  donatedValue: number;
  status: NeedStatus;
  socialWorkerName: string;
  socialWorkerPhone: string;
  socialWorkerEmail: string;
  supervisorName: string;
  familyName: string;
  deliveryAddress: string;
  county: string;
  requestType: string;
  itemType: string;
  itemsRequested: string;
  requestedAt?: number;
  gender: Genders[];
  ageRange: AgeRanges[];
  claimedAt?: number;
  itemsOrdered: string;
  deliverDetails: string;
  receiverName: string;
  pickupName: string;
  notes: string;
  championID?: number;
  thankYouSent?: 0 | 1;
  champion?: ChampionInterfaces;
}

export interface ResponseUserNeeds {
  errorCode: ErrorInterface;
  response?: UserNeedInterface[];
}

export interface ResponseUserNeed {
  errorCode: ErrorInterface;
  response?: UserNeedInterface & {relatedNeeds?: UserNeedInterface};
}

export interface NeedsStatsInterface {
  "listed": number;
  "met": number;
}
export interface ResponseUserNeedsStats {
  errorCode: ErrorInterface;
  response?: NeedsStatsInterface;
}
