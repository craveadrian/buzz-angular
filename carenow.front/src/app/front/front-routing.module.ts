import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomepageComponent} from "./pages/homepage/homepage.component";
import {MainComponent} from "./components/layout/main/main.component";
import {ListNeedsComponent} from "./pages/needs/list-needs/list-needs.component";
import {DetailsNeedComponent} from "./pages/needs/details-need/details-need.component";
import {ThankYouNeedComponent} from "./pages/needs/thank-you-need/thank-you-need.component";
import {UnsubscribeComponent} from "./pages/unsubscribe/unsubscribe.component";
import {SubscribeComponent} from "./pages/subscribe/subscribe.component";

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: '',
        component: HomepageComponent
      },
      {
        path: 'needs',
        component: ListNeedsComponent
      },
      {
        path: 'needs/:id',
        component: DetailsNeedComponent
      },
      {
        path: 'thank-you/:id',
        component: ThankYouNeedComponent,
        data: {
          itemShare: false
        }
      },
      {
        path: 'share-need/:id',
        component: ThankYouNeedComponent,
        data: {
          itemShare: true
        }
      },
      {
        path: 'unsubscribe',
        component: UnsubscribeComponent,
        data: {
          itemShare: true
        }
      },
      {
        path: 'subscribe',
        component: SubscribeComponent,
        data: {
          itemShare: true
        }
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FrontRoutingModule { }
