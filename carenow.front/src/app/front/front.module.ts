import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomepageComponent } from './pages/homepage/homepage.component';
import {FrontRoutingModule} from "./front-routing.module";
import { MainComponent } from './components/layout/main/main.component';
import { MenuComponent } from './components/layout/menu/menu.component';
import { FooterComponent } from './components/layout/footer/footer.component';
import {CoreModule} from "../@core/@core.module";
import { PrezentationVideoComponent } from './components/prezentation-video/prezentation-video.component';
import { NewsletterComponent } from './components/newsletter/newsletter.component';
import { ListNeedsComponent } from './pages/needs/list-needs/list-needs.component';
import { DetailsNeedComponent } from './pages/needs/details-need/details-need.component';
import { ThankYouNeedComponent } from './pages/needs/thank-you-need/thank-you-need.component';
import { MeetThisNeedComponent } from './components/meet-this-need/meet-this-need.component';
import {JoinSelectPipe} from "./_pipes/join-select.pipe";
import {PaginationModule} from "ngx-bootstrap/pagination";
import {CarouselModule} from "ngx-owl-carousel-o";
import { UnsubscribeComponent } from './pages/unsubscribe/unsubscribe.component';
import { SubscribeComponent } from './pages/subscribe/subscribe.component';

@NgModule({
  declarations: [
    HomepageComponent,
    MainComponent,
    MenuComponent,
    FooterComponent,
    PrezentationVideoComponent,
    NewsletterComponent,
    ListNeedsComponent,
    DetailsNeedComponent,
    ThankYouNeedComponent,
    MeetThisNeedComponent,
    JoinSelectPipe,
    UnsubscribeComponent,
    SubscribeComponent
  ],
  imports: [
    CommonModule,
    FrontRoutingModule,
    CoreModule,
    PaginationModule,
    CarouselModule
  ]
})
export class FrontModule { }
