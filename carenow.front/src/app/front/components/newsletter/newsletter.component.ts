import {Component, HostBinding, OnDestroy} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {Subject, takeUntil} from "rxjs";
import {ApiService} from "../../../@core/_services/api.service";
import {LoaderService} from "../../../@core/_services/loader.service";
import {UsersService} from "../../_services/users.service";


@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.scss']
})
export class NewsletterComponent implements OnDestroy {
  @HostBinding('class') classes = 'bg-primary';
  form: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  destroySubject$: Subject<void> = new Subject();

  constructor(
    private http: HttpClient,
    private apiService: ApiService,
    private loaderService: LoaderService,
    private usersService: UsersService,
  ) {

    this.form = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      firstName: new FormControl(null,[Validators.required]),
      lastName: new FormControl(null,[Validators.required]),
    })
  }

  get f() { return this.form.controls; }

  submit() {
    this.error = '';
    if (this.form.status === 'VALID') {

      this.loading = true;
      this.loaderService.loading.next(true);

      const data = {...this.form.value};

      this.usersService.subscribe(data)
        .pipe(takeUntil(this.destroySubject$))
        .subscribe(
          {
            next: () => {
              this.loading = false;
              this.submitted = true;
            },
            error: (error) => this.apiService.handleError(error),
            complete: () => this.apiService.handleComplete()
          }
        )
    }
  }

  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.unsubscribe();
  }
}
