import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subject, takeUntil} from "rxjs";
import {BsModalRef} from "ngx-bootstrap/modal";
import {UserNeedInterface} from "../../_interfaces/needs.interfaces";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import * as moment from 'moment';
import {FrontNeedsService} from "../../_services/front-needs.service";
import {ApiService} from "../../../@core/_services/api.service";
import {LoaderService} from "../../../@core/_services/loader.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-meet-this-need',
  templateUrl: './meet-this-need.component.html',
  styleUrls: ['./meet-this-need.component.scss']
})
export class MeetThisNeedComponent implements OnDestroy, OnInit {
  @Input() item: UserNeedInterface | undefined;
  public onClose: Subject<any> = new Subject();

  form: FormGroup;
  submitted: boolean = false;

  destroySubject$: Subject<void> = new Subject();

  constructor(
    public bsModalRef: BsModalRef,
    private frontNeedsService: FrontNeedsService,
    private apiService: ApiService,
    private loaderService: LoaderService,
    private router: Router,
  ) {
    this.form = new FormGroup({
      id: new FormControl(null, [Validators.required]),
      firstName: new FormControl(null, [Validators.required]),
      lastName: new FormControl(null, [Validators.required]),
      email: new FormControl(null, [Validators.required]),
      phoneNumber: new FormControl(null, [Validators.required]),
      agreePurchase: new FormControl(null, [Validators.requiredTrue]),
      agreeTerms: new FormControl(null, [Validators.requiredTrue])
    });
  }

  ngOnInit(): void
  {
    if (this.item && this.item.id) {
      this.form.controls['id'].setValue(this.item.id);
    }
  }

  get f() { return this.form.controls; }

  save(): void {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    this.loaderService.loading.next(true);

    const data = {...this.form.value};

    delete data.agreePurchase;
    delete data.agreeTerms;

    this.frontNeedsService.claimNeed(data)
      .pipe(takeUntil(this.destroySubject$))
      .pipe(
        this.apiService.parsErrors()
      )
      .subscribe({
        next: async (need: UserNeedInterface | null) => {
          this.loaderService.loading.next(false);
          if (need) {
            this.onClose.next(true);
            this.bsModalRef.hide();
            await this.router.navigate(
              ['/thank-you', data.id]
            );

          }
        },
        error: (error) => this.apiService.handleError(error),
        complete: () => this.apiService.handleComplete()
      });


  }

  closeModal(): void
  {
    this.onClose.next(false);
    this.bsModalRef.hide();
  }

  formatDate(
    unixTime: number
  ): string {
    return moment.unix(unixTime).format("MMM Do, YYYY");
  }
  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.unsubscribe();
  }

}
