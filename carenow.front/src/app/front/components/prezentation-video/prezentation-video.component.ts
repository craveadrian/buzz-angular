import { Component } from '@angular/core';
import {BsModalRef} from "ngx-bootstrap/modal";

@Component({
  selector: 'app-prezentation-video',
  templateUrl: './prezentation-video.component.html',
  styleUrls: ['./prezentation-video.component.scss']
})
export class PrezentationVideoComponent {

  constructor(
    public bsModalRef: BsModalRef,
  ) { }

}
