import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject, takeUntil} from "rxjs";
import {FrontNeedsService} from "../../../_services/front-needs.service";
import {ApiService} from "../../../../@core/_services/api.service";
import {NeedsStatsInterface} from "../../../_interfaces/needs.interfaces";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit, OnDestroy {
  needsStats: NeedsStatsInterface;
  destroySubject$: Subject<void> = new Subject();

  constructor(
    private frontNeedsService: FrontNeedsService,
    private apiService: ApiService,
  ) {
    this.needsStats = frontNeedsService.needsStats;
  }

  ngOnInit(): void {
    this.getNeedsStats();
  }

  getNeedsStats(): void {
    this.frontNeedsService.getNeedsStats()
      .pipe(takeUntil(this.destroySubject$))
      .pipe(
        this.apiService.parsErrors()
      )
      .subscribe({
        next: (needsStats: NeedsStatsInterface | null) => {
          if (needsStats) {
            this.needsStats = needsStats;
          }
        },
        error: (error) => this.apiService.handleError(error),
        complete: () => this.apiService.handleComplete()
      });
  }

  onOpenChange(event: any): void {
    console.log(event);
    // this.menuOpen = event
  }

  ngOnDestroy():void {
    this.destroySubject$.next();
    this.destroySubject$.unsubscribe();
  }
}
