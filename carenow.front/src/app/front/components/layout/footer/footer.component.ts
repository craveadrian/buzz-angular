import {Component, HostBinding} from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent  {
  year: number;
  @HostBinding('class') classes = 'bg-gray';
  constructor() {
    this.year = new Date().getFullYear();
  }


}
