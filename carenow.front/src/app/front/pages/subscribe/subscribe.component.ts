import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject, takeUntil} from "rxjs";
import {UsersService} from "../../_services/users.service";
import {ActivatedRoute} from "@angular/router";
import {ApiService} from "../../../@core/_services/api.service";
import {LoaderService} from "../../../@core/_services/loader.service";

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.scss']
})
export class SubscribeComponent implements OnDestroy {
  loading: boolean | undefined = true;
  email: string | undefined;
  destroySubject$: Subject<void> = new Subject();

  constructor(
    private usersService: UsersService,
    private activatedRoute: ActivatedRoute,
    private apiService: ApiService,
    private loaderService: LoaderService,
  ) {
    this.loaderService.loading.next(true);
    this.activatedRoute.queryParams
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((params) => {
        let email = params['email'];
        if (email) {
          this.email = email;
          this.subscribe();
        }
      });
  }

  subscribe(): void {
    this.loading = true;
    this.loaderService.loading.next(true);
    if (this.email) {
      this.usersService.subscribe({email:this.email})
        .pipe(takeUntil(this.destroySubject$))
        .subscribe(
          {
            next: () => {
              this.loading = false;
            },
            error: (error) => this.apiService.handleError(error),
            complete: () => this.apiService.handleComplete()
          }
        )
    }
  }

  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.unsubscribe();
  }
}
