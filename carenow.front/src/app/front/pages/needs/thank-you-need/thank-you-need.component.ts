import {Component, OnDestroy} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {FrontNeedsService} from "../../../_services/front-needs.service";
import {ApiService} from "../../../../@core/_services/api.service";
import {BsModalService} from "ngx-bootstrap/modal";
import {Subject, takeUntil} from "rxjs";
import {NeedStatus, UserNeedInterface} from "../../../_interfaces/needs.interfaces";
import {environment} from "../../../../../environments/environment";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-thank-you-need',
  templateUrl: './thank-you-need.component.html',
  styleUrls: ['./thank-you-need.component.scss']
})
export class ThankYouNeedComponent implements OnDestroy{
  item: UserNeedInterface | undefined;
  itemsLoading = true;
  itemShare: boolean = true;

  frontUrl:string = environment.frontUrl;
  destroySubject$: Subject<void> = new Subject();

  constructor(
    private route: ActivatedRoute,
    private frontNeedsService: FrontNeedsService,
    private apiService: ApiService,
    private modalService: BsModalService,
    private router: Router,
    private sanitizer:DomSanitizer
  ) {
    this.route.paramMap.subscribe((params: ParamMap) => {
      const itemId = params.get('id');
      if (itemId) {
        this.getNeed(parseInt(itemId));
      }
    })

    this.itemShare = route.snapshot.data['itemShare'];

  }

  sanitize(url:string){
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  needShareLink(
    Id: number
  ): string {
    return (!this.itemShare)? (this.frontUrl + 'share-need/' + Id) : (this.frontUrl + 'needs');
  }

  private getNeed(
    id: number
  ): void
  {
    this.frontNeedsService.getNeed(id)
      .pipe(takeUntil(this.destroySubject$))
      .pipe(
        this.apiService.parsErrors()
      )
      .subscribe({
        next: async (need: (UserNeedInterface)| null) => {
          this.itemsLoading = false;
          if (
            need &&
            !(need.status === NeedStatus.draft || need.status === NeedStatus.listed)
          ) {
            this.item = need;
          } else {
            await this.router.navigate(['/needs']);
          }
        },
        error: (error) => this.apiService.handleError(error),
        complete: () => this.apiService.handleComplete()
      });
  }

  ngOnDestroy():void {
    this.destroySubject$.next();
    this.destroySubject$.unsubscribe();
  }
}
