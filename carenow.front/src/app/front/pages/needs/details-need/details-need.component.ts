import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {Subject, takeUntil} from "rxjs";
import {UserNeedInterface} from "../../../_interfaces/needs.interfaces";
import {FrontNeedsService} from "../../../_services/front-needs.service";
import {ApiService} from "../../../../@core/_services/api.service";
import {environment} from "../../../../../environments/environment";
import {PrezentationVideoComponent} from "../../../components/prezentation-video/prezentation-video.component";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {OwlOptions} from "ngx-owl-carousel-o";
import {MeetThisNeedComponent} from "../../../components/meet-this-need/meet-this-need.component";
import {NeedInterface} from "../../../../admin/_interfaces/needs.interfaces";

@Component({
  selector: 'app-details-need',
  templateUrl: './details-need.component.html',
  styleUrls: ['./details-need.component.scss']
})
export class DetailsNeedComponent implements OnInit, OnDestroy {
  public emptySlider: number = 0;
  @ViewChild('container') container : ElementRef |  undefined;
  item: UserNeedInterface & {relatedNeeds?: (UserNeedInterface & {days:number})[]} | undefined;
  itemsLoading = true;
  customOptions: OwlOptions = {
    loop: false,
    mouseDrag: false,
    touchDrag: true,
    pullDrag: true,
    dots: true,
    navSpeed: 700,
    margin:24,
    navText: [
      '<img  src="./assets/images/icons/icon-slide-arrow-left.svg" alt="slide left">',
      '<img  src="./assets/images/icons/icon-slide-arrow-right.svg" alt="slide right">'
    ],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    },
    nav: true
  }
  frontUrl:string = environment.frontUrl;
  destroySubject$: Subject<void> = new Subject();

  @HostListener('window:resize', ['$event'])
  onResize(): void {
    if (this.container) {
      const htmlElement: HTMLElement = this.container.nativeElement;
      this.setEmptySlider(htmlElement.clientWidth);
    }

  }

  constructor(
    private route: ActivatedRoute,
    private frontNeedsService: FrontNeedsService,
    private apiService: ApiService,
    private modalService: BsModalService,
    private router: Router
  ) {
    this.route.paramMap
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((params: ParamMap) => {
      const itemId = params.get('id');
      if (itemId) {
        this.getNeed(parseInt(itemId));
      }
    })
  }

  ngOnInit(): void {
    if (this.container) {
      const htmlElement: HTMLElement = this.container.nativeElement;
      this.setEmptySlider(htmlElement.clientWidth);
    }
  }

 async meetThisNeed(
   item: (NeedInterface & any)
 ): Promise<void>
  {

    const initialState:  Partial<MeetThisNeedComponent> = {
      item: item
    };

    const modalRef: BsModalRef = this.modalService.show(MeetThisNeedComponent, {
      initialState,
      class: 'modal-md',
      backdrop : 'static',
      keyboard : false
    })

    await modalRef.content.onClose
      .pipe(takeUntil(this.destroySubject$))
      .subscribe( async (result: boolean) => {
        if (result) {
          await this.router.navigate(['/thank-you', item.id],{state: {data: {itemShare: false}}});
        } else {
          // modal is closed, no actions
        }
      })

  }

  presentationVideo(): void
  {
    this.modalService.show(PrezentationVideoComponent, {
      class: 'modal-lg'
    })
  }

  private setEmptySlider(
    width: number
  ): void {

    const relatedItemsNr = (this.item?.relatedNeeds)? this.item?.relatedNeeds.length : 0;


    if ( width < 400  ) {
      this.emptySlider = 0;
    }

    if (width >= 400 && width < 740  ) {
      if(relatedItemsNr) {
        this.emptySlider = (relatedItemsNr === 1)? 1 : 0;
      }
    }

    if (width >= 740 && width < 940  ) {
      if(relatedItemsNr) {
        this.emptySlider = (relatedItemsNr < 3)? (3-relatedItemsNr) : 0;
      }
      // 3 items
    }

    if (width >= 940) {
      if(relatedItemsNr) {
        this.emptySlider = (relatedItemsNr < 4)? (4-relatedItemsNr) : 0;
      }
    }

  }

  private getNeed(
    id: number
  ): void
  {
    this.frontNeedsService.getNeed(id)
      .pipe(takeUntil(this.destroySubject$))
      .pipe(
        this.apiService.parsErrors()
      )
      .subscribe({
        next: async (need: (UserNeedInterface & {relatedNeeds?: (UserNeedInterface & {days:number})[]})| null) => {
          this.itemsLoading = false;
          if (need) {
            if (need.relatedNeeds && need.relatedNeeds.length > 0) {
              this.parsNeeds(need.relatedNeeds).then((needs) => {
                const data: any = {...need};
                data.relatedNeeds = needs;
                this.item = data;
              });
            } else {
              this.item = need;
            }
          } else {
            await this.router.navigate(['/needs']);
          }
        },
        error: (error) => this.apiService.handleError(error),
        complete: () => this.apiService.handleComplete()
      });
  }

  async parsNeeds(
    needs: UserNeedInterface[]
  ): Promise<(UserNeedInterface & {days?: number})[]> {
    return needs.map( (data) => {
      const res = {...data, days: 0};
      if (data.expiresAt) {
        res.days = this.frontNeedsService.transform(data.expiresAt * 1000)
      }
      return res;
    })
  }

  ngOnDestroy():void {
    this.destroySubject$.next();
    this.destroySubject$.unsubscribe();
  }

}
