import {Component, OnDestroy, OnInit} from '@angular/core';
import {FrontNeedsService} from "../../../_services/front-needs.service";
import {NeedsSearchInterfaces, UserNeedInterface} from "../../../_interfaces/needs.interfaces";
import {Observable, of, Subject, takeUntil} from "rxjs";
import {ApiService} from "../../../../@core/_services/api.service";
import {PageChangedEvent} from "ngx-bootstrap/pagination";
import {environment} from "../../../../../environments/environment";

@Component({
  selector: 'app-list-needs',
  templateUrl: './list-needs.component.html',
  styleUrls: ['./list-needs.component.scss']
})
export class ListNeedsComponent implements OnInit, OnDestroy {
  items: (UserNeedInterface & {days?: number})[] | undefined;
  itemsLoading = true;
  itemsLoadingSearch = false;
  currentPage = 1;
  totalItems: number = 0;
  page = 1;

  ageRange$ = this.frontNeedsService.ageRange$;
  price$ = this.frontNeedsService.price$;
  gender$ = this.frontNeedsService.gender$;
  expiresAt$ = this.frontNeedsService.expiresAt$;

  counties$: Observable<{id: string, name: string}[]> | undefined;
  counties: {id: string, name: string}[] | undefined;
  countiesLoading: boolean = true;

  searchNeeds: NeedsSearchInterfaces = {
    ageRange: [],
    skip: 0,
    limit: 36
  };

  frontUrl:string = environment.frontUrl;
  destroySubject$: Subject<void> = new Subject();

  constructor(
    private frontNeedsService: FrontNeedsService,
    private apiService: ApiService
  ) { }

  ngOnInit(): void {
    this.getNeedsList();
    this.getNeedsListCounter();
    this.getCounties();
  }

  search(): void {
    this.itemsLoadingSearch = true;
    this.resetPagination();
    this.getNeedsList();
    this.getNeedsListCounter();
  }

  resetPagination(): void {
    this.searchNeeds.skip = 0;
    this.currentPage = 1;
  }

  onPageChange(
    event: PageChangedEvent,
    el: HTMLElement
  ): void {
    this.itemsLoadingSearch = true;
    this.searchNeeds.skip = (event.page -1 ) * (this.searchNeeds.limit?? 12);
    this.getNeedsList();
    this.scroll(el);
  }

  scroll(el: HTMLElement): void {
    el.scrollIntoView();
  }

   private getNeedsList(): void
  {
    this.frontNeedsService.getNeeds(this.searchNeeds)
      .pipe(takeUntil(this.destroySubject$))
      .pipe(
        this.apiService.parsErrors()
      )
      .subscribe({
        next: async (needs: (UserNeedInterface[] & {id:number})| null) => {
          this.itemsLoading = false;
          this.itemsLoadingSearch = false;
          if (needs) {
            this.parsNeeds(needs).then((needs) => {
              this.items = needs;
            });
          }
        },
        error: (error) => this.apiService.handleError(error),
        complete: () => this.apiService.handleComplete()
      });
  }

  async parsNeeds(
    needs: UserNeedInterface[]
  ): Promise<(UserNeedInterface & {days?: number})[]> {
    return needs.map( (data) => {
      const res = {...data, days: 0};
      if (data.expiresAt) {
        res.days = this.frontNeedsService.transform(data.expiresAt * 1000)
      }
      return res;
    })
  }

  private getNeedsListCounter(): void
  {
    this.frontNeedsService.getNeedsCounter(this.searchNeeds)
      .pipe(takeUntil(this.destroySubject$))
      .pipe(
        this.apiService.parsErrors()
      )
      .subscribe({
        next: (needsCounter: number | null ) => {
          if (needsCounter !== null) {
            this.totalItems = needsCounter?? 0;
          }
        },
        error: (error) => this.apiService.handleError(error),
        complete: () => this.apiService.handleComplete()
      });
  }

  private getCounties(): void
  {
    this.frontNeedsService.getCounties()
      .pipe(takeUntil(this.destroySubject$))
      .pipe(
        this.apiService.parsErrors()
      )
      .subscribe({
        next: (countries: string[] | null) => {

          if (countries) {
            let list = countries.filter((item) => {
              return !!(item);
            })
            const data = list.map( value => {
              return {id:value, name: value};
            })
            this.counties$ = of(data);
            this.counties = data;
            this.countiesLoading = false;
          }
        },
        error: (error) => this.apiService.handleError(error),
        complete: () => this.apiService.handleComplete()
      });
  }





  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.unsubscribe();
  }

}
