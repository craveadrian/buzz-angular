import {Component, OnDestroy} from '@angular/core';
import {Subject, takeUntil} from "rxjs";
import {UsersService} from "../../_services/users.service";
import {ActivatedRoute} from "@angular/router";
import {ApiService} from "../../../@core/_services/api.service";
import {LoaderService} from "../../../@core/_services/loader.service";

@Component({
  selector: 'app-unsubscribe',
  templateUrl: './unsubscribe.component.html',
  styleUrls: ['./unsubscribe.component.scss']
})
export class UnsubscribeComponent implements OnDestroy {
  loading: boolean | undefined = true;
  email: string | undefined;
  destroySubject$: Subject<void> = new Subject();

  constructor(
    private usersService: UsersService,
    private activatedRoute: ActivatedRoute,
    private apiService: ApiService,
    private loaderService: LoaderService,
  ) {
    this.loaderService.loading.next(true);
    this.activatedRoute.queryParams
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((params) => {
      let email = params['email'];
      if (email) {
        this.email = email;
        this.unSubscribe();
      }
    });
  }

  unSubscribe(): void {
    this.loading = true;
    this.loaderService.loading.next(true);
    if (this.email) {
      this.usersService.unsubscribe(this.email)
        .pipe(takeUntil(this.destroySubject$))
        .subscribe(
          {
            next: () => {
              this.loading = false;
            },
            error: (error) => this.apiService.handleError(error),
            complete: () => this.apiService.handleComplete()
          }
        )
    }
  }

  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.unsubscribe();
  }

}
