import {Component, OnDestroy, OnInit} from '@angular/core';
import {BsModalService} from "ngx-bootstrap/modal";
import {Subject, takeUntil} from "rxjs";
import {PrezentationVideoComponent} from "../../components/prezentation-video/prezentation-video.component";
import {FrontNeedsService} from "../../_services/front-needs.service";
import {ApiService} from "../../../@core/_services/api.service";
import {environment} from "../../../../environments/environment";
import {UserNeedInterface} from "../../_interfaces/needs.interfaces";

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit, OnDestroy {
  items: (UserNeedInterface)[] | undefined;
  frontUrl:string = environment.frontUrl;
  destroySubject$: Subject<void> = new Subject();

  constructor(
    private modalService: BsModalService,
    private frontNeedsService: FrontNeedsService,
    private apiService: ApiService
  ) { }

  ngOnInit(): void
  {
    this.dotsAnimation();
    this.getNeedsList();
  }

  presentationVideo(): void
  {
    this.modalService.show(PrezentationVideoComponent, {
      class: 'modal-lg'
    })
  }

  private getNeedsList(): void
  {
    this.frontNeedsService.getFeaturedNeeds()
      .pipe(takeUntil(this.destroySubject$))
      .pipe(
        this.apiService.parsErrors()
      )
      .subscribe({
        next: (needs: (UserNeedInterface[] & {id:number})| null) => {
          if (needs) {
            this.items = needs;
          }
        },
        error: (error) => this.apiService.handleError(error),
        complete: () => this.apiService.handleComplete()
      });
  }

  private dotsAnimation(): void {
      let dots = document.querySelectorAll<HTMLElement>(".dots-hero .dot");

      const levers = Array.from(dots).map(function () {
        return Math.max(Math.random(), .3);
      });

    const max = 350;
    window.addEventListener("mousemove", function (e) {
      const locX = e.clientX / window.innerWidth - .5;
      const locY = e.clientY / window.innerHeight - .5;

      Array.from(dots).forEach(function (dot, i) {
        const x = levers[i] * max * - locX;
        const y = levers[i] * max * - locY;
          dot.style.transform = "translateX(".concat(String(x), "px) translateY(").concat(String(y), "px)");
        });
      });

  }

  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.unsubscribe();
  }
}
