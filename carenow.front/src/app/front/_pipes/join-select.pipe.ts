import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'joinSelect'
})
export class JoinSelectPipe implements PipeTransform {

  transform(
    input:Array<any>,
    sep = ', '
  ): string {

    let data = input.map((value) => {
      return value.name;
    });

    const string = data.join(sep);

    return JoinSelectPipe.truncate(string, 24);
  }

  private static truncate(str: string, length: number){
    return (str.length > length) ? str.substr(0, length-1) + '...' : str;
  };

}
