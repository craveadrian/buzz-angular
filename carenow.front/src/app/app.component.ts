import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject, takeUntil} from "rxjs";
import {LoaderService} from "./@core/_services/loader.service";

@Component({
  selector: 'app-root',
  template: `
    <router-outlet></router-outlet>
    <div class="my-overlay" *ngIf="showOverlay">
      <fa-icon [icon]="'spinner'" class="fa-spin"></fa-icon>
    </div>
  `
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'carenow';
  showOverlay = false;

  destroySubject$: Subject<void> = new Subject();

  constructor(
    private loaderService: LoaderService
  ) {

  }

  ngOnInit(): void {
    this.loaderService.loading
      .pipe(takeUntil(this.destroySubject$))
      .subscribe(
        (val) => {
          this.showOverlay = val;
        }
      );
  }

  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.unsubscribe();
  }

}
