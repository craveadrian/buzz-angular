import { NgModule } from '@angular/core';
import {ToastrModule} from "ngx-toastr";
import {FontAwesomeModule, FaIconLibrary} from "@fortawesome/angular-fontawesome";
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {InlineSVGModule} from "ng-inline-svg-2";
import {NgSelectModule} from "@ng-select/ng-select";
import {BsDatepickerModule} from "ngx-bootstrap/datepicker";
import {QuillModule} from "ngx-quill";
import {ImageCropperModule} from "ngx-image-cropper";
import {ModalModule} from "ngx-bootstrap/modal";
import {BsDropdownModule} from "ngx-bootstrap/dropdown";
import {ShimmerModule} from "@sreyaj/ng-shimmer";


const modules = {
  syntax: true,
  toolbar: [
    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
    ['blockquote', 'code-block'],

    [{ 'header': 1 }, { 'header': 2 }],               // custom button values
    [{ 'list': 'ordered'}, { 'list': 'bullet' }],
    [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
    [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
    [{ 'direction': 'rtl' }],                         // text direction

    [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

    // [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
    [{ 'font': [] }],
    [{ 'align': [] }],

    ['clean'],                                         // remove formatting button

    ['link', 'image', 'video']                         // link and image, video
  ]
};

@NgModule({
  declarations: [],
  imports: [
    ToastrModule.forRoot(),
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    InlineSVGModule.forRoot(),
    BsDatepickerModule.forRoot(),
    NgSelectModule,
    QuillModule.forRoot({
      modules
    }),
    ImageCropperModule,
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    ShimmerModule
  ],
  exports: [
    ToastrModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    InlineSVGModule,
    NgSelectModule,
    BsDatepickerModule,
    QuillModule,
    ImageCropperModule,
    ModalModule,
    BsDropdownModule,
    ShimmerModule
  ]
})
export class CoreModule {
  constructor(private library: FaIconLibrary) {
    library.addIconPacks(fas, far, fab);
  }
}
