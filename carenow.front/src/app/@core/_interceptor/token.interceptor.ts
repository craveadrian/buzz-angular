import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CookieCustomService} from "../_services/cookie.custom.service";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(
        private localStorage: CookieCustomService,
    ) {
    }

    public  getToken(): string | null {
        const token = this.localStorage.getItem('token');
        return (token)? token : null;
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let serverReq: HttpRequest<any> = request;
        if (request) {

            const token = this.getToken();

            if (token && (request.url.indexOf('s3.') === -1)) {
                Object.assign(request, {
                    setHeaders: {
                        Authorization: token
                    }
                });
            }


            serverReq = request.clone(request);
        }

        return next.handle(serverReq);
    }
}
