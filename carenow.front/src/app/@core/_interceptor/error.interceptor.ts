import {Observable, throwError} from 'rxjs';
import {tap, catchError} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {
    HttpClient,
    HttpErrorResponse,
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
} from '@angular/common/http';
import {Router} from '@angular/router';
import {ToastrService} from "ngx-toastr";

import {CookieCustomService} from "../_services/cookie.custom.service";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private http: HttpClient,
    private router: Router,
    private localStorage: CookieCustomService,
    private toastr: ToastrService
  ) {
  }

  intercept(
      request: HttpRequest<any>,
      next: HttpHandler
  ): Observable<HttpEvent<any>> {
        return next.handle(request)
        .pipe(
          tap(),
          catchError((error: HttpErrorResponse) => {
            let errorMessage = `Error: ${error.error.message}`;
            if (error.status === 402) {
                return throwError((() => error));
            } else if (error.status === 400) {
              this.toastr.error(`${error.error.error.message || errorMessage}`);
            } else if (error.status === 422) {
              this.toastr.error(`Invalid parameters!`);
            } else if (error.status === 404) {
              this.toastr.error(`Url is not valid!`);
            } else if (error.status === 401) {
                this.toastr.error(`Authorization required`);
                this.logout();
            } else if (error.status === 0) {
              this.toastr.error(`Connection refused.`);
            } else if (error.error && error.error.error) {
              this.toastr.error(`${error.error.error.message || errorMessage}`);
            } else {
              this.toastr.error(`${errorMessage}`);
            }
            return throwError((() => error));
          })
        );
  }

  logout(): void {
    this.localStorage.clear();
    this.router.navigate(['/']).then();
  }
}

