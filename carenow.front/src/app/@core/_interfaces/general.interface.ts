export interface ErrorInterface {
  code: number;
  name?: string;
  message: string;
}

export interface ResponseDropdown {
  errorCode: ErrorInterface;
  response?: string[];
}

export interface ResponseCounter {
  errorCode: ErrorInterface;
  response?: number;
}

export interface ResponseRemove {
  errorCode: ErrorInterface;
}

export interface ResponseOk {
  errorCode: ErrorInterface;
}

export interface ResponseLogout {
  errorCode: ErrorInterface;
}
export interface ResponseDownloadToken {
  errorCode: ErrorInterface;
  response?: string;
}
