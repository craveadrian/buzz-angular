import {Injectable} from '@angular/core';
import {CookieService} from "ngx-cookie-service";

@Injectable({
  providedIn: 'root'
})
export class CookieCustomService {
  public expireDate: Date = new Date(new Date().getTime() + 24*60*60*1000); //next Day
  constructor(
    private cookieService: CookieService
  ) {
  }

  public getItem(name: string): string | null {
    if (['token'].indexOf(name)!==-1 || ['deviceToken'].indexOf(name)!==-1){
      return this.cookieService.get(name);
    }
    return localStorage.getItem(name);

  }

  public setItem(
    name: string,
    value: string,
    expiredAd: number | null = null
  ) {
    this.setExpireCookieDate();
    if (['token'].indexOf(name)!==-1 || ['deviceToken'].indexOf(name)!==-1  ){
      const expired = (expiredAd)? new Date(expiredAd * 1000) : this.expireDate;
      this.cookieService.set(name, value, expired,'/',window.location.hostname,false,"Strict");
    }
    else{
      localStorage.setItem(name,value);
    }
  }
  public clear() {
    localStorage.clear();
    this.cookieService.deleteAll('/',window.location.hostname);
  }

  private setExpireCookieDate() {
    const ttl = parseInt(this.cookieService.get('ttl'));
    if (ttl && !isNaN(ttl)) {
      this.expireDate = new Date(new Date().getTime() + ttl * 1000);
    }
  }
}
