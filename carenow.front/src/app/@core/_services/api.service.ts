import { Injectable } from '@angular/core';
import {map} from "rxjs/operators";
import {ToastrService} from "ngx-toastr";
import {ResponseUserLogin} from "../../admin/_interfaces/users.interfaces";
import {Router} from "@angular/router";
import {LoaderService} from "./loader.service";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  submitted: boolean | undefined;

  constructor(
    private toastr: ToastrService,
    private router: Router,
    private loaderService: LoaderService,
  ) {
  }

  public parsErrors<T>() {
    return map((res: any) => {
      if (res.errorCode.code === 0) {
        return res.response?? true;
      } else {
        if (res && res.errorCode && res.errorCode.message) {
          this.toastr.error(`${res.errorCode.message}`);
        } else {
          this.toastr.error(`General Error`);
        }
        return null;
      }
    });
  }

  public handleLoginResponse<T>(
    data: ResponseUserLogin
  ) {
    if(data.response && data.response.user) {
      this.loaderService.loading.next(true);
      this.router.navigate(['/admin/dashboard/']);
    } else {
      this.toastr.error(data.errorCode.message);
    }
  }

  public handleError<T>(
    error: any
  ) {
    console.log("handleError");
    console.log(error);
    this.loaderService.loading.next(false);
    this.toastr.error(error.message);
  }

  public handleComplete<T>() {
    this.loaderService.loading.next(false);
  }

}
