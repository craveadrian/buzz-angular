import { Injectable } from '@angular/core';
import {HttpClient, HttpEvent, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {lastValueFrom, Observable} from "rxjs";
import * as uuid from "uuid";
import {ErrorInterface} from "../../@core/_interfaces/general.interface";

export interface GenerateAwsSignedPostURLInterface {
  errorCode: ErrorInterface;
  response?: {
    url: string;
    fields: {
      Key: string;
      bucket: string;
      ['X-Amz-Algorithm']: string;
      ['X-Amz-Credential']: string;
      ['X-Amz-Date']: string;
      Policy: string;
      ['X-Amz-Signature']: string;
    }
  }
}
const headers = new HttpHeaders({
  'Content-Type': 'application/json'
});

@Injectable({
  providedIn: 'root'
})
export class AwsFilesService {

  constructor(
    protected http: HttpClient
  ) {
  }

  getCustomSignedUrl(
    type: string,
    key: string,
    size: number,
    temp: boolean = true
  ): Observable<GenerateAwsSignedPostURLInterface> {
    if (temp) {
      return this.http.post<GenerateAwsSignedPostURLInterface>(environment.apiUrl + '/aws/generateTempSignedPostURL',
        {type, key, size}, {headers}
      );
    } else {
      return this.http.post<GenerateAwsSignedPostURLInterface>(environment.apiUrl + '/aws/generateDirectUploadURL',
        {type, key, size}, {headers}
      );
    }
  }

  async uploadFileAWSS3(
    responseGet: any, file: any
  ): Promise<HttpEvent<any>> {
    const formData = new FormData();
    formData.append('key', responseGet.fields.Key);
    formData.append('X-Amz-Algorithm', responseGet.fields['X-Amz-Algorithm']);
    formData.append('X-Amz-Credential', responseGet.fields['X-Amz-Credential']);
    formData.append('X-Amz-Date', responseGet.fields['X-Amz-Date']);
    formData.append('Policy', responseGet.fields.Policy);
    formData.append('X-Amz-Signature', responseGet.fields['X-Amz-Signature']);
    formData.append('Content-Type', file.type);
    formData.append('file', file);

    let url = responseGet.url;

    const httpOptions = {
      headers: {
        'Access-Control-Allow-Origin': '*',
        Accept: 'application/x-www-form-urlencoded'
      }
    };
    const uploadFile$ = this.http.post<any>(url, formData, httpOptions);
    return await lastValueFrom(uploadFile$);
  }


  async uploadFile(
    file: File | null,
    folderStorage: string,
    temp: boolean = true
  ): Promise<string | null > {
    if (!file) {
      return null;
    }

    const extension = file.type.split('/').pop();
    const fileName = uuid.v4() + '.' + extension;

    let key = ((temp)?'tmp/': '') + folderStorage + '/' + fileName;

    const customSignedUrl$ = this.getCustomSignedUrl(file.type, key, file.size, temp);
    const customSignedUrl = await lastValueFrom(customSignedUrl$);

    if (!(customSignedUrl && customSignedUrl.response)) {
      return null;
    }
    return this.uploadFileAWSS3(customSignedUrl.response, file)
      .then( (resUpload) => {
        return (temp)?key.slice(4): key;
      })
      .catch( (error) => {
        console.log("error")
        console.log(error)
        return null;
      })


  }
}
