import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {
  NeedBulkUpdateInterface,
  NeedInterface,
  NeedsSearchInterfaces,
  ResponseNeed,
  ResponseNeeds
} from "../_interfaces/needs.interfaces";
import {Observable, of} from "rxjs";
import {CustomHttpParamEncoder} from "../../@core/decoder/custom.http.param.decoder";
import {environment} from "../../../environments/environment";
import {ResponseCounter, ResponseDropdown, ResponseOk, ResponseRemove} from "../../@core/_interfaces/general.interface";
import {HelpersService} from "./helpers.service";

const headers = new HttpHeaders({
  'Content-Type': 'application/json'
});

@Injectable({
  providedIn: 'root'
})
export class AdminNeedsService {

  status$: Observable<{id: string, name: string}[]> = of([
    {id: 'draft', name: 'Draft'},
    {id: 'listed', name: 'Listed'},
    {id: 'claimed', name: 'Claimed'},
    {id: 'purchased', name: 'Purchased'},
    {id: 'delivered', name: 'Delivered'},
    {id: 'pickedUp', name: 'PickedUp'}
  ])

  needType$: Observable<{id: string, name: string}[]> = of([
    {id: 'emergency/group home', name: 'Emergency/Group Home'},
    {id: 'pre-custody', name: 'Pre-custody'},
    {id: 'kinship placement', name: 'Kinship Placement'},
    {id: 'aged-out youth', name: 'Aged-out Youth'},
    {id: 'reunification', name: 'Reunification'},
    {id: 'foster family', name: 'Foster Family'},
  ])

  itemType$: Observable<{id: string, name: string}[]> = of([
    {id: 'Clothing', name: 'Clothing'},
    {id: 'Bed', name: 'Bed'},
    {id: 'Household Items', name: 'Household Items'},
    {id: 'Diapers', name: 'Diapers'},
    {id: 'Groceries', name: 'Groceries'},
    {id: 'Baby Gear', name: 'Baby Gear'},
    {id: 'Car Seat', name: 'Car Seat'},
    {id: 'Toiletries', name: 'Toiletries'},
    {id: 'Furniture', name: 'Furniture'},
    {id: 'Gas', name: 'Gas'},
    {id: 'Formula', name: 'Formula'},
    {id: 'Other', name: 'Other'}
  ])

  ageRange$: Observable<{id: string, name: string}[]> = of([
    {id: 'baby', name: 'Baby (0-12 mos)'},
    {id: 'toddler', name: 'Toddler (1-3 yrs)'},
    {id: 'preschool', name: 'Preschool (3-5 yrs)'},
    {id: 'gradeschool', name: 'Gradeschool (5-12 yrs)'},
    {id: 'teen', name: 'Teen (12-18 yrs)'},
    {id: 'adult', name: 'Adult (18+ yrs)'}
  ])

  gender$: Observable<{id: string, name: string}[]> = of([
    {id: 'male', name: 'Male'},
    {id: 'female', name: 'Female'}
  ])

  constructor(
    private http: HttpClient,
    private helper: HelpersService
  ) { }

  public getNeed(
    id: number
  ): Observable<ResponseNeed> {

    let params = new HttpParams({encoder: new CustomHttpParamEncoder()});
    params = params.append('id', id);

    return this.http.get<ResponseNeed>(environment.apiUrl + '/admin/needs/getObject',
      {params: params, headers})
      ;
  }

  public getNeeds(
    options: NeedsSearchInterfaces
  ): Observable<ResponseNeeds> {

    let params = this.SearchFilters(options);

    if (options.order) {
      params = params.append('order', options.order);
    }

    if (options.limit) {
      params = params.append('limit', options.limit);
    }

    if (options.skip) {
      params = params.append('skip', options.skip);
    }

    return this.http.get<ResponseNeeds>(environment.apiUrl + '/admin/needs/getObjects',
      {params: params, headers})
      ;
  }

  public getNeedsCounter(
    options: NeedsSearchInterfaces
  ): Observable<ResponseCounter> {

    let params = this.SearchFilters(options);

    return this.http.get<ResponseCounter>(environment.apiUrl + '/admin/needs/getObjectsCounter',
      {params: params, headers:headers}
    );
  }

  public getCounties(): Observable<ResponseDropdown>
  {
    return this.http.get<ResponseDropdown>(environment.apiUrl + '/admin/needs/getCounties',
      {headers:headers}
    );
  }

  public removeNeed(
    id: number
  ): Observable<ResponseRemove> {
    const params = new HttpParams({encoder: new CustomHttpParamEncoder()})
      .set('id', id);

    return this.http.get<ResponseRemove>(environment.apiUrl + '/admin/needs/removeObject',
      {params: params, headers})
      ;
  }

  public saveNeed (
    need: Partial<NeedInterface>
  ): Observable<ResponseNeed> {
    this.helper.clean(need);
    return this.http.post<ResponseNeed>(environment.apiUrl + '/admin/needs/saveObject', need,
      {headers}
    );
  }

  public bulkUpdate (
    need: NeedBulkUpdateInterface
  ): Observable<ResponseOk> {
    this.helper.clean(need);
    return this.http.post<ResponseOk>(environment.apiUrl + '/admin/needs/bulkUpdate', need,
      {headers}
    );
  }

   SearchFilters(
    options: NeedsSearchInterfaces
  ) {
    let params = new HttpParams({encoder: new CustomHttpParamEncoder()});

    if (options.keyword) {
      params = params.append('keyword', options.keyword);
    }
    if (options.county) {
      params = params.append('county', options.county);
    }
    if (options.status) {
      params = params.append('status', options.status);
    }

    if (options.thankYouSent) {
      params = params.append('thankYouSent', options.thankYouSent);
    }

    return params;
  }
}
