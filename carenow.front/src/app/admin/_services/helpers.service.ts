import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HelpersService {

  constructor() { }

  base64ToFile(data: any, filename: any) {
    const arr = data.split(',');
    const mime = arr[0].match(/:(.*?);/)[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    let u8arr = new Uint8Array(n);

    while(n--){
      u8arr[n] = bstr.charCodeAt(n);
    }

    return new File([u8arr], filename, { type: mime });
  }

  // todo refactor

  clean(obj: any): any {
    // clean array
    if (Array.isArray(obj)) {
      for (let i=0; i<obj.length; i++) {
        if (this.isNothing(obj[i])) obj.splice(i, 1);  // remove value if falsy
        else if (typeof obj[i] === 'object') this.clean(obj[i]); // recurse if it's a truthy object
      }

      // clean other object
    } else {
      for (let prop in obj) {
        if (!obj.hasOwnProperty(prop)) continue;
        if (this.isNothing(obj[prop])) delete obj[prop]; // remove value if falsy
        else if (typeof obj[prop] === 'object') this.clean(obj[prop]); // recurse if it's a truthy object
      }
    }
  }

  isNothing(item: any): any {
    // null / undefined
    if (item == null) return true;

    // deep object falsiness
    if (typeof item === 'object') {
      if (Array.isArray(item)) {
        // array -> check for populated/nonnull value
        for (let i=0; i<item.length; i++) {
          if (!this.isNothing(item[i])) return false;
        }
        return true;
      }
      // other object -> check for populated/nonnull value
      for (let prop in item) {
        if (!item.hasOwnProperty(prop)) continue;
        if (!this.isNothing(item[prop])) return false;
      }
      return true;
    }
    return false;
  }

}
