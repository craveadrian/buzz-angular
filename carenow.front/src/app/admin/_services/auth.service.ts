import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";
import {CookieCustomService} from "../../@core/_services/cookie.custom.service";
import {ResponseUserLogin, UserInterface, UserLogin} from "../_interfaces/users.interfaces";
import {ResponseDownloadToken, ResponseLogout} from "../../@core/_interfaces/general.interface";

@Injectable({
    providedIn: 'root'
})
export class AuthService {
  constructor(
      private http: HttpClient,
      private cookieService: CookieCustomService,
  ) {
  }

  logout(): Observable<ResponseLogout> {
      return this.http.get<ResponseLogout>(environment.apiUrl + '/admin/users/logout', {});
  }

  getDownloadToken(): Observable<ResponseDownloadToken>
  {
      return this.http.get<ResponseDownloadToken>(environment.apiUrl + '/admin/users/getDownloadToken', {});
  }

  loginAdmin(
    formData: UserLogin
  ): Observable<ResponseUserLogin> {
    return this.http.post<ResponseUserLogin>(environment.apiUrl + '/admin/users/login', formData)
      .pipe(
        map((res: ResponseUserLogin) => {
          if (res.response && res.response.id) {
            const data = res.response;
            let expiredAt = (((new Date().getTime() / 1000) | 0) + data.ttl);
            this.cookieService.setItem('currentUser', JSON.stringify(data.user));
            this.cookieService.setItem('token', data.id, expiredAt);
            this.cookieService.setItem('ttl', data.ttl.toString());
            this.cookieService.setItem('expiredAt', expiredAt.toString());
          }
          return res;
        }
    ));
  }

  getUserRole(): string {
    return JSON.parse(<string>this.cookieService.getItem('currentUser')).role;
  }

  getUserInfo(): UserInterface {
    return JSON.parse(<string>this.cookieService.getItem('currentUser'));
  }

  adminAuthenticated(): boolean {
    return !!(this.cookieService.getItem('token') && this.cookieService.getItem('currentUser') &&
        JSON.parse(<string>this.cookieService.getItem('currentUser')).role === 'admin');
  }

}
