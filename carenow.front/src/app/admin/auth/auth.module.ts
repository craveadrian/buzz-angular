import { NgModule } from '@angular/core';
import { LoginComponent } from './pages/login/login.component';
import { AuthRoutingModule } from "./auth-routing.module";
import {CoreModule} from "../../@core/@core.module";



@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    AuthRoutingModule,
    CoreModule
  ]
})
export class AuthModule { }
