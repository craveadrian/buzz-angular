import {Component, OnDestroy} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Subject, takeUntil} from "rxjs";
import {LoaderService} from "../../../../@core/_services/loader.service";
import {UserLogin} from "../../../_interfaces/users.interfaces";
import {AuthService} from "../../../_services/auth.service";
import {ApiService} from "../../../../@core/_services/api.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnDestroy {
  form: FormGroup;
  submitted: boolean = false;

  destroySubject$: Subject<void> = new Subject();

  constructor(
    private loaderService: LoaderService,
    private authService: AuthService,
    private apiService: ApiService
  ) {
    this.form = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null,
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(16)
        ]
      ),
    })
  }

  get f() { return this.form.controls; }

  onSubmit() {
    this.loaderService.loading.next(true);
    this.submitted = true;
    if (this.form.invalid) {
      this.loaderService.loading.next(false);
      return;
    }

    const data: UserLogin = {
      email: this.form.value.email,
      password: this.form.value.password
    }
    this.authService.loginAdmin(data)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe(
        {
          next: (data) => this.apiService.handleLoginResponse(data),
          error: (error) => this.apiService.handleError(error),
          complete: () => this.apiService.handleComplete()
        }
      )
  }

  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.unsubscribe();
  }

}
