import {Component, OnDestroy} from '@angular/core';
import {NeedBulkUpdateInterface, NeedInterface, NeedsSearchInterfaces} from "../../../_interfaces/needs.interfaces";
import {Subject, takeUntil} from "rxjs";
import {AdminNeedsService} from "../../../_services/admin-needs.service";
import {ToastrService} from "ngx-toastr";
import {ApiService} from "../../../../@core/_services/api.service";
import {PageChangedEvent} from "ngx-bootstrap/pagination";
import {environment} from "../../../../../environments/environment";
import {LoaderService} from "../../../../@core/_services/loader.service";
import {AuthService} from "../../../_services/auth.service";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {GroupNeedsComponentComponent} from "../../components/group-needs-component/group-needs-component.component";

@Component({
  selector: 'app-needs-list',
  templateUrl: './needs-list.component.html',
  styleUrls: ['./needs-list.component.scss']
})
export class NeedsListComponent implements OnDestroy {
  searchOpen = false;
  loading = false;
  selectAllNeeds: null | boolean  = null;
  items: (NeedInterface & {selected?: boolean})[] | undefined;

  currentPage = 1;
  totalItems: number = 0;
  page = 1;

  search: NeedsSearchInterfaces = {
    order: 'id DESC',
    limit: 30,
    skip: 0,
  };

  tableHead = [
    {label: "Id", colName: "id", total: true, sort: true, selectCheckbox: true},
    {label: "Title", colName: "title", total: false, sort: true},
    {label: "Status", colName: "status", total: false, sort: true},
    {label: "Due Date", colName: "expiresAt", total: false, sort: true},
    {label: "Family Name", colName: "familyName", total: false, sort: true},
    {label: "Social Worker Name", colName: "socialWorkerName", total: false, sort: true},
    {label: "Supervisor Name", colName: "supervisorName", total: false, sort: true},
    {label: "Actions", sort: false},
  ]

  destroySubject$: Subject<void> = new Subject();

  constructor(
    private adminNeedsService: AdminNeedsService,
    private toastr: ToastrService,
    private apiService: ApiService,
    private loaderService: LoaderService,
    private authService: AuthService,
    private modalService: BsModalService,
  ) {
    this.getNeedsList();
    this.getNeedsListCounter();
  }

  selectNeed(
    need: NeedInterface & {selected?: boolean}
  ): void {
    need.selected = !need.selected;
  }

  async selectAllNeedsAction(
    $event: Event
  ): Promise<void> {
    $event.stopPropagation();
    const target = $event.target as HTMLInputElement;
    if (target.checked) {
      this.items?.map((item) => {
        item.selected = true;
        return item;
      })
    } else {
      this.items?.map((item) => {
        item.selected = false;
        return item;
      })
    }
  }

  async selectItem(
    $event: Event
  ): Promise<void> {
    const target = $event.target as HTMLInputElement;
    const needsId: any = await this.selectedNeeds();

    if (target.checked) {
      if (this.items?.length === (needsId.length + 1)) {
        this.selectAllNeeds = true;
      }
    } else {
      if (this.selectAllNeeds) {
        if (this.items?.length !== (needsId.length + 1)) {
          this.selectAllNeeds = false;
        }
      }
    }
  }

  async groupNeed(): Promise<void>
  {

    const modalRef: BsModalRef = this.modalService.show(GroupNeedsComponentComponent, {
      class: 'modal-lg'
    })

    let data:any = {};
    modalRef.content.data
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((result: any) => {
      data = result;
    })
    await modalRef.content.save
      .pipe(takeUntil(this.destroySubject$))
      .subscribe( async (result: any) => {
      if (result) {
        this.bulkUpdateNeeds(data);
      }

    })
  }


  async bulkUpdateNeeds(
    data: Partial<NeedBulkUpdateInterface>
  ): Promise<void> {

    const needsId: any = await this.selectedNeeds();

    if ( needsId && needsId.length > 0) {
      const needsBulUpdate: any = {
        ids: needsId,
        ...data
      }

      this.adminNeedsService.bulkUpdate(needsBulUpdate)
        .pipe(takeUntil(this.destroySubject$))
        .pipe(
          this.apiService.parsErrors()
        )
        .subscribe({
          next: () => {
            this.resetPagination();
            this.getNeedsList();
            this.getNeedsListCounter();
          },
          error: (error) => this.apiService.handleError(error),
          complete: () => this.apiService.handleComplete()
        });
    }

  }

  async selectedNeeds(): Promise<any[] | undefined>
  {
    return  this.items?.filter( (item: NeedInterface & {selected?: boolean } ) => {
      return !!(item.selected);
    }).map( (item) => {
      return item.id;
    });
  }


  getNeedsList(): void {
    this.loaderService.loading.next(true);
    this.adminNeedsService.getNeeds(this.search)
      .pipe(takeUntil(this.destroySubject$))
      .pipe(
        this.apiService.parsErrors()
      )
      .subscribe({
        next: (needs: (NeedInterface[] & {id:number})| null) => {
          if (needs) {
            this.items = needs;
          }
        },
        error: (error) => this.apiService.handleError(error),
        complete: () => this.apiService.handleComplete()
      });
  }

  getNeedsListCounter(): void {
    this.adminNeedsService.getNeedsCounter(this.search)
      .pipe(takeUntil(this.destroySubject$))
      .pipe(
        this.apiService.parsErrors()
      )
      .subscribe({
      next: (needsCounter: number | null) => {
        this.totalItems = needsCounter?? 0;
      },
      error: (error) => this.apiService.handleError(error),
      complete: () => this.apiService.handleComplete()
    });
  }

  trackByFn(index: number): number {
    return index;
  }

  onPageChange(event: PageChangedEvent): void {
    this.search.skip = (event.page -1 ) * (this.search.limit?? 10);
    this.getNeedsList();
    this.selectAllNeeds = false;
  }

  order(order: string | null): void {
    if (order) {
      this.search.order = order;
      this.resetPagination();
    } else {
      this.search.order = null;
      this.resetPagination();
    }
    this.getNeedsList();
  }

  searchForm(
    searchValues: NeedsSearchInterfaces
  ): void {
    this.search = {...this.search, ...searchValues };
    this.resetPagination();
    this.getNeedsList();
    this.getNeedsListCounter();
  }

  toggle() {
    this.searchOpen = !this.searchOpen;
  }

  remove(
    id: number | undefined
  ) {
    if (id && confirm("Are you sure that you want to remove this item?")) {
      this.loaderService.loading.next(true);
      this.adminNeedsService.removeNeed(id)
        .pipe(takeUntil(this.destroySubject$))
        .pipe(
          this.apiService.parsErrors()
        )
        .subscribe({
          next: () => {
            this.resetPagination();
            this.getNeedsList();
            this.getNeedsListCounter();
          },
          error: (error) => this.apiService.handleError(error),
          complete: () => this.apiService.handleComplete()
        });
    }
  }

  needLink(
    item: NeedInterface
  ) : string {
    return environment.frontUrl + '/needs/' + item.id;
  }

  private resetPagination(): void {
    this.search.skip = 0;
    this.currentPage = 1;
    this.selectAllNeeds = false;
  }

  getDownloadToken(): void
  {
    this.loaderService.loading.next(true);
    this.authService.getDownloadToken()
      .pipe(takeUntil(this.destroySubject$))
      .pipe(
        this.apiService.parsErrors()
      )
      .subscribe({
        next: (token: string | null | undefined) => {
          if (token) {
            let urlParams = this.adminNeedsService.SearchFilters(this.search);
            urlParams = urlParams.append('access_token', token);
            const link = environment.apiUrl + '/admin/needs/download?' + urlParams.toString();
            window.open(link, '_blank');
          }
        },
        error: (error) => this.apiService.handleError(error),
        complete: () => this.apiService.handleComplete()
      });
  }

  ngOnDestroy() {
    this.destroySubject$.next();
    this.destroySubject$.unsubscribe();
  }


}
