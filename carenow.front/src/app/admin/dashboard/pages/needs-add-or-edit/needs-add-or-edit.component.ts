import {Component, OnDestroy} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {LoaderService} from "../../../../@core/_services/loader.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Observable, of, Subject, takeUntil} from "rxjs";
import {AwsFilesService} from "../../../_services/aws-files.service";
import {BsDatepickerConfig} from "ngx-bootstrap/datepicker";
import * as uuid from "uuid";
import {NeedInterface} from "../../../_interfaces/needs.interfaces";
import {environment} from "../../../../../environments/environment";
import {AdminNeedsService} from "../../../_services/admin-needs.service";
import {ApiService} from "../../../../@core/_services/api.service";
import {CropImageComponentComponent} from "../../components/crop-image-component/crop-image-component.component";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";

@Component({
  selector: 'app-needs-add-or-edit',
  templateUrl: './needs-add-or-edit.component.html',
  styleUrls: ['./needs-add-or-edit.component.scss']
})
export class NeedsAddOrEditComponent implements OnDestroy {
  item: NeedInterface | undefined;

  loading = false;
  form: FormGroup;
  colorTheme:string =  'theme-blue';
  bsConfigDay: Partial<BsDatepickerConfig> = {
    containerClass: this.colorTheme,
    dateInputFormat: 'YYYY-MM-DD'
  }
  toolbarOptions = [
    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
    ['blockquote', 'code-block'],

    [{ 'header': 1 }, { 'header': 2 }],               // custom button values
    [{ 'list': 'ordered'}, { 'list': 'bullet' }],
    [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
    [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
    [{ 'direction': 'rtl' }],                         // text direction

    [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

    // [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
    [{ 'font': [] }],
    [{ 'align': [] }],
    ['link'],
    ['clean'],                                         // remove formatting button

  ];

  quillConfigDefault = {
    modules: {
      toolbar: {
        container: this.toolbarOptions
      }
    },
    scrollingContainer: '.scrolling-container', // optional
    placeholder: 'Compose your article',
    theme: 'snow',
  };
  status$ = this.adminNeedsService.status$;
  needType$ = this.adminNeedsService.needType$;
  itemType$ = this.adminNeedsService.itemType$;
  ageRange$ = this.adminNeedsService.ageRange$;
  gender$ = this.adminNeedsService.gender$;

  imageUpload: {base64: string, file: File | null} | undefined | null;
  counties$: Observable<string[]> | undefined;
  countiesLoading: boolean = true;
  folderStorage = 'needs/needs_' + environment.env + '_'+ uuid.v4();

  destroySubject$: Subject<void> = new Subject();

  constructor(
    private route: ActivatedRoute,
    private loaderService: LoaderService,
    private formBuilder: FormBuilder,
    private awsFilesService: AwsFilesService,
    private adminNeedsService: AdminNeedsService,
    private apiService: ApiService,
    private modalService: BsModalService,
    private router: Router
  ) {

    this.form = new FormGroup({
      title: new FormControl(null, [Validators.required]),
      expiresAt: new FormControl(null, [Validators.required]),
      description: new FormControl(null, [Validators.required]),
      status: new FormControl(null, [Validators.required]),
      price: new FormControl(null, []),
      donatedValue: new FormControl(null, []),
      socialWorkerName: new FormControl(null, []),
      campaignURL: new FormControl(null, []),
      imageURL: new FormControl(null, []),
      familyName: new FormControl(null, []),
      socialWorkerEmail: new FormControl(null, []),
      county: new FormControl(null, [Validators.required]),
      socialWorkerPhone: new FormControl(null, []),
      deliveryAddress: new FormControl(null, []),
      supervisorName: new FormControl(null, []),
      requestedAt: new FormControl(null, []),
      requestType: new FormControl(null, []),
      itemType: new FormControl(null, []),
      gender: new FormControl(null, []),
      ageRange: new FormControl(null, []),
      itemsRequested: new FormControl(null, []),
      champion: new FormGroup({
          phoneNumber: new FormControl(null, []),
          firstName: new FormControl(null, []),
          lastName: new FormControl(null, []),
          email: new FormControl(null, []),
          id: new FormControl(null, []),
        }),
      itemsOrdered: new FormControl(null, []),
      deliverDetails: new FormControl(null, []),
      pickupName: new FormControl(null, []),
      receiverName: new FormControl(null, []),
      thankYouSent: new FormControl(null, []),
      notes: new FormControl(null, []),
      storageFolder: new FormControl(this.folderStorage, [Validators.required]),
    });

    this.route.paramMap.subscribe((params: ParamMap) => {
      const itemId = params.get('id');
      if (itemId) {
        this.getNeed(parseInt(itemId));
      }
    });
    this.getCounties();

  }
  get f() { return this.form.controls; }


  async save(): Promise<void> {

    if(this.form.invalid) {
      this.form.markAllAsTouched();
      setTimeout(() => {
          const classElement = document.getElementsByClassName('is-invalid');
          if(classElement.length > 0){
            classElement[0].scrollIntoView();
          }
        }
      , 100);

      return;
    }

    this.loaderService.loading.next(true);
    const data = {...this.form.value};

    if (data.expiresAt) {
      const end = new Date(data.expiresAt);
      end.setHours(23,59,59,999);
      data.expiresAt = (end.getTime() / 1000) | 0;
    } else {
      data.expiresAt = null
    }

    if (data.requestedAt) {
      const end = new Date(data.requestedAt);
      end.setHours(23,59,59,999);
      data.requestedAt = (end.getTime() / 1000) | 0;
    } else {
      data.requestedAt = null
    }
    data.thankYouSent =  ((data.thankYouSent)? 1 : 0);

    if (this.imageUpload  && this.imageUpload.file) {
      data.imageURL = await this.awsFilesService.uploadFile(this.imageUpload?.file, this.folderStorage);
    } else {
      data.imageURL = this.item?.imageURL;
    }

    this.adminNeedsService.saveNeed(data)
      .pipe(takeUntil(this.destroySubject$))
      .pipe(
        this.apiService.parsErrors()
      )
      .subscribe({
        next: (countries: string[] | null) => {
          if (countries) {
            this.router.navigate([ '/admin/dashboard']);
          }
        },
        error: (error) => this.apiService.handleError(error),
        complete: () => this.apiService.handleComplete()
      });
  }

  async handleFileCrop(
    fileEvent: Event
  ) {

    const files = (fileEvent.target as HTMLInputElement).files;

    if (files && files.length > 0) {

      let aspectRatio = 16/9;

      const initialState:  Partial<CropImageComponentComponent> = {
        options: {maintainAspectRation: true, aspectRatio: aspectRatio},
        imageToCrop: fileEvent,
        imageName: files.item(0)?.name ?? undefined
      };

      const modalRef: BsModalRef = this.modalService.show(CropImageComponentComponent, {
        initialState,
        class: 'modal-lg'
      })

      let croppedImage:any = {};
      modalRef.content.imageCroppedEvent
        .pipe(takeUntil(this.destroySubject$))
        .subscribe((result: any) => {
        croppedImage = result;
      })
      await modalRef.content.onClose
        .pipe(takeUntil(this.destroySubject$))
        .subscribe( async (result: any) => {

        if (result) {
          this.imageUpload = croppedImage;
        } else {
          this.form.controls['imageURL'].setValue(null);
        }
      })
    }
  }

  removeDefaultImage() {
    this.imageUpload = null;
  }

  setDefaultCounty(): void
  {
    this.form.get('county')?.setValue('Mecklenburg');
  }

  setDeliveryAddress(): void {
    const address = 'DSS';
    this.form.get('deliveryAddress')?.setValue(address);
  }

  private getCounties(): void
  {
    this.loaderService.loading.next(true);
    this.adminNeedsService.getCounties()
      .pipe(takeUntil(this.destroySubject$))
      .pipe(
        this.apiService.parsErrors()
      )
      .subscribe({
        next: (countries: string[] | null) => {
          if (countries) {
            let list = countries.filter((item) => {
              return !!(item);
            })
            // const data = list.map( value => {
            //   return {id:value, name: value};
            // })
            this.counties$ = of(list);
            this.countiesLoading = false;
          }
        },
        error: (error) => this.apiService.handleError(error),
        complete: () => this.apiService.handleComplete()
      });
  }

  private getNeed(
    id: number
  ): void {
    this.loaderService.loading.next(true);
    this.adminNeedsService.getNeed(id)
      .pipe(takeUntil(this.destroySubject$))
      .pipe(
        this.apiService.parsErrors()
      )
      .subscribe({
        next: (needs: NeedInterface | null) => {
          if (needs) {
            this.editNeed(needs);
          }
        },
        error: (error) => this.apiService.handleError(error),
        complete: () => this.apiService.handleComplete()
      });
  }

  private editNeed(
    item: NeedInterface
  ): void {
    this.item = item;

    if (this.item) {

      if (this.item.storageFolder) {
        this.folderStorage = this.item.storageFolder;
      }

      this.form.addControl('id', new FormControl(this.item.id));

      this.form.get('title')?.setValue(this.item.title);
      this.form.get('description')?.setValue(this.item.description);
      if (this.item.expiresAt) {
        this.form.get('expiresAt')?.setValue(new Date(this.item.expiresAt * 1000));
      }
      this.form.get('status')?.setValue(this.item.status);
      this.form.get('price')?.setValue(this.item.price);
      this.form.get('donatedValue')?.setValue(this.item.donatedValue);
      this.form.get('campaignURL')?.setValue(this.item.campaignURL);
      this.form.get('storageFolder')?.setValue(this.item.storageFolder);
      this.form.get('socialWorkerName')?.setValue(this.item.socialWorkerName);
      this.form.get('familyName')?.setValue(this.item.familyName);
      this.form.get('socialWorkerEmail')?.setValue(this.item.socialWorkerEmail);
      this.form.get('county')?.setValue(this.item.county);
      this.form.get('socialWorkerPhone')?.setValue(this.item.socialWorkerPhone);
      this.form.get('deliveryAddress')?.setValue(this.item.deliveryAddress);
      this.form.get('supervisorName')?.setValue(this.item.supervisorName);
      this.form.get('requestType')?.setValue(this.item.requestType);
      this.form.get('itemType')?.setValue(this.item.itemType);
      this.form.get('gender')?.setValue(this.item.gender);
      this.form.get('ageRange')?.setValue(this.item.ageRange);
      this.form.get('itemsRequested')?.setValue(this.item.itemsRequested);


      this.form.get('champion')?.get('phoneNumber')?.setValue(this.item.champion?.phoneNumber);
      this.form.get('champion')?.get('firstName')?.setValue(this.item.champion?.firstName);
      this.form.get('champion')?.get('lastName')?.setValue(this.item.champion?.lastName);
      this.form.get('champion')?.get('email')?.setValue(this.item.champion?.email);
      this.form.get('champion')?.get('id')?.setValue(this.item.champion?.id);

      this.form.get('itemsOrdered')?.setValue(this.item.itemsOrdered);
      this.form.get('deliverDetails')?.setValue(this.item.deliverDetails);
      this.form.get('notes')?.setValue(this.item.notes);
      this.form.get('receiverName')?.setValue(this.item.receiverName);
      this.form.get('pickupName')?.setValue(this.item.pickupName);
      this.form.get('thankYouSent')?.setValue((this.item.thankYouSent)? 1 : 0);

      if (this.item.requestedAt) {
        this.form.get('requestedAt')?.setValue(new Date(this.item.requestedAt * 1000));
      }
      if (this.item.imageURL) {
        this.imageUpload = {base64: environment.frontUrl + this.item.imageURL, file: null};
      }

    }

  }

  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.unsubscribe();
  }
}
