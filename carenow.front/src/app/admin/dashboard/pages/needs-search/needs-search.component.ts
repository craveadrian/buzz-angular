import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {NeedsSearchInterfaces} from "../../../_interfaces/needs.interfaces";
import {FormControl, FormGroup} from "@angular/forms";
import {Observable, of, Subject, takeUntil} from "rxjs";
import {AdminNeedsService} from "../../../_services/admin-needs.service";
import {ToastrService} from "ngx-toastr";
import {ApiService} from "../../../../@core/_services/api.service";

@Component({
  selector: 'app-needs-search',
  templateUrl: './needs-search.component.html',
  styleUrls: ['./needs-search.component.scss']
})
export class NeedsSearchComponent implements OnInit, OnDestroy {
  @Output() searchOut: EventEmitter<NeedsSearchInterfaces> = new EventEmitter<NeedsSearchInterfaces>();
  tracking:  any;
  form: FormGroup;
  submitted: boolean = false;

  status$ = this.adminNeedsService.status$;

  counties$: Observable<{id: string, name: string}[]> | undefined;
  countiesLoading: boolean = true;

  destroySubject$: Subject<void> = new Subject();

  constructor(
    private adminNeedsService: AdminNeedsService,
    private toastr: ToastrService,
    private apiService: ApiService
  ) {
    this.form = new FormGroup({
      keyword: new FormControl(null, []),
      county: new FormControl(null, []),
      status: new FormControl(null, []),
      thankYouSentYes: new FormControl(null, []),
      thankYouSentNo: new FormControl(null, [])
    })
  }

  ngOnInit(): void {
    this.getCounties();
  }

  private getCounties(): void {
    this.adminNeedsService.getCounties()
      .pipe(takeUntil(this.destroySubject$))
      .pipe(
        this.apiService.parsErrors()
      )
      .subscribe({
        next: (countries: string[] | null) => {
          if (countries) {
            const data = countries.map( value => {
              return {id:value, name: value};
            })
            this.counties$ = of(data);
            this.countiesLoading = false;
          }
        },
        error: (error) => this.apiService.handleError(error),
        complete: () => this.apiService.handleComplete()
      });
  }


  get f() {
    return this.form.controls;
  }

  search(): void {
    this.searchOut.emit(this.parsValues());
  }

  searchEnter() {
    this.startTrackingLoop();
  }

  startTrackingLoop() {
    if (this.tracking) {
      clearTimeout(this.tracking);
    }
    this.tracking = setTimeout(() => {
      this.searchOut.emit(this.parsValues());
    }, 1000);
  }

  reset(): void {
    this.searchOut.emit(this.parsValues(true));

  }

  parsValues(reset?: boolean): NeedsSearchInterfaces {
    if(reset) {
      this.form.reset();
    }
    const data = {...this.form.value};

    if(data.keyword) {
      data.keyword = data.keyword.trim();
    }

    data.thankYouSent = null
    if (data.thankYouSentYes) {
      data.thankYouSent = 1;
    }
    if (data.thankYouSentNo) {
      data.thankYouSent = 0;
    }

    return data;
  }

  ngOnDestroy() {
    this.destroySubject$.next();
    this.destroySubject$.unsubscribe();
  }

}
