import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from "../../../../_services/auth.service";
import {Subject, takeUntil} from "rxjs";
import {ApiService} from "../../../../../@core/_services/api.service";
import {CookieCustomService} from "../../../../../@core/_services/cookie.custom.service";
import {LoaderService} from "../../../../../@core/_services/loader.service";
import {ToastrService} from "ngx-toastr";

export interface MenuInterface {
  title: string;
  icon: string;
  link: string;
}

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, OnDestroy {
  @Input() menu: MenuInterface[] | undefined;
  openMenu = false;

  destroySubject$: Subject<void> = new Subject();

  constructor(
    private authService:AuthService,
    private apiService: ApiService,
    private cookieService: CookieCustomService,
    private loaderService: LoaderService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.menu = [
      {
        title: 'Needs',
        icon: './assets/images/icons/icon-need.svg',
        link: '/admin/dashboard'
      }
    ]

  }

  logout() {
    this.loaderService.loading.next(true);
    this.authService.logout()
      .pipe(takeUntil(this.destroySubject$))
      .pipe(
        this.apiService.parsErrors()
      )
      .subscribe({
        next: () => {
          this.cookieService.clear();
          location.reload();
        },
        error: (error) => this.apiService.handleError(error),
        complete: () => this.apiService.handleComplete()
      });
  }

  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.unsubscribe();
  }
}
