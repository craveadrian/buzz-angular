import {Component, Input, OnInit} from '@angular/core';
import {ImageCroppedEvent} from "ngx-image-cropper";
import {Subject} from "rxjs";
import {BsModalRef} from "ngx-bootstrap/modal";
import {HelpersService} from "../../../_services/helpers.service";

@Component({
  selector: 'app-crop-image-component',
  templateUrl: './crop-image-component.component.html',
  styleUrls: ['./crop-image-component.component.scss']
})
export class CropImageComponentComponent implements OnInit {
  @Input() options: any;

  public aspectRatio = 16/9;
  public onClose: Subject<any>;
  public imageCroppedEvent: Subject<any>;

  cropImageExist = false;

  imageToCrop: any;
  imageName: any;

  croppedImage: any = '';
  croppedImageBlob: any = '';

  constructor(
    public bsModalRef: BsModalRef,
    private helper: HelpersService
  ) {
    this.onClose = new Subject();
    this.imageCroppedEvent = new Subject();
  }

  ngOnInit(): void {
    this.cropImageExist = true;
    this.onClose.next(false);
    this.aspectRatio = this.options.aspectRatio;
  }

  imageCropped(
    event: ImageCroppedEvent
  ): void {
    this.croppedImage = event.base64;
    const file = this.helper.base64ToFile(event.base64, this.imageName);
    this.imageCroppedEvent.next({base64:event.base64, file: file})
  }

  imageLoaded(): void
  {
    this.cropImageExist = true;
  }

  sendFileToParent(): void
  {
    this.onClose.next(true);
    this.bsModalRef.hide()
  }

  closeModal(): void
  {
    this.onClose.next(false);
    this.bsModalRef.hide();
  }

}
