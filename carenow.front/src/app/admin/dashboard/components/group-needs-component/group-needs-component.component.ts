import { Component, OnInit } from '@angular/core';
import {Subject} from "rxjs";
import {BsModalRef} from "ngx-bootstrap/modal";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-group-needs-component',
  templateUrl: './group-needs-component.component.html',
  styleUrls: ['./group-needs-component.component.scss']
})
export class GroupNeedsComponentComponent {
  public save: Subject<boolean> = new Subject();
  public data: Subject<any> = new Subject();

  form: FormGroup;
  constructor(
    public bsModalRef: BsModalRef,
  ) {
    this.form = new FormGroup({
      familyName: new FormControl(null, []),
    });
  }

  groupNeedSave(): void
  {
    const data = {...this.form.value};
    this.data.next(data);
    this.save.next(true);
    this.bsModalRef.hide()
  }

  closeModal(): void
  {
    this.save.next(false);
    this.bsModalRef.hide();
  }
}
