import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DashboardRoutingModule} from "./dashboard-routing.module";
import { NeedsListComponent } from './pages/needs-list/needs-list.component';
import { MainComponent } from './components/layout/main/main.component';
import { SidebarComponent } from './components/layout/sidebar/sidebar.component';
import {CoreModule} from "../../@core/@core.module";
import {AccordionModule} from "ngx-bootstrap/accordion";
import {PaginationModule} from "ngx-bootstrap/pagination";
import { NeedsSearchComponent } from './pages/needs-search/needs-search.component';
import { NeedsAddOrEditComponent } from './pages/needs-add-or-edit/needs-add-or-edit.component';
import { CropImageComponentComponent } from './components/crop-image-component/crop-image-component.component';
import { GroupNeedsComponentComponent } from './components/group-needs-component/group-needs-component.component';



@NgModule({
  declarations: [
    NeedsListComponent,
    MainComponent,
    SidebarComponent,
    NeedsSearchComponent,
    NeedsAddOrEditComponent,
    CropImageComponentComponent,
    GroupNeedsComponentComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    CoreModule,
    AccordionModule,
    PaginationModule.forRoot()
  ]
})
export class DashboardModule { }
