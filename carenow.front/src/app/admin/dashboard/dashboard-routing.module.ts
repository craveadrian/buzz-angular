import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {NeedsListComponent} from "./pages/needs-list/needs-list.component";
import {MainComponent} from "./components/layout/main/main.component";
import {NeedsAddOrEditComponent} from "./pages/needs-add-or-edit/needs-add-or-edit.component";

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: '',
        component: NeedsListComponent
      },
      {
        path: 'need',
        component: NeedsAddOrEditComponent
      },
      {
        path: 'need/:id',
        component: NeedsAddOrEditComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
