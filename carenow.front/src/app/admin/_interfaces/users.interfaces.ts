import {ErrorInterface} from "../../@core/_interfaces/general.interface";

export enum UserRolesEnum {
  admin = "admin",
  user = "user"
}
export enum UserStatusEnum {
  active = "active",
  disabled = "disabled",
  removed = "removed",
}

export interface UserLogin {
  email: string;
  password: string;
}

export interface UserInterface {
  email: string;
  username: string;
  role: UserRolesEnum;
  storageFolder: string;
  avatarURL: null | string;
  status: null | UserStatusEnum;
  createdAt: number,
  updatedAt: number,
  lastLogin: number
}

export interface UserLoginInterface {
  id: string;
  type: string;
  ttl: number;
  createdAt: number;
  userID: number;
  user: UserInterface;
}

export interface ResponseUserLogin {
  errorCode: ErrorInterface;
  response?: UserLoginInterface;
}
