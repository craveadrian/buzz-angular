export enum NotificationStatus {
  enabled = "enabled",
  disabled = "disabled"
}

export interface ChampionInterfaces {
  id: number,
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  notificationStatus: NotificationStatus;
  createdAt: number;
  updatedAt: number;
  donations: number;
  claimCounter: number;
}
