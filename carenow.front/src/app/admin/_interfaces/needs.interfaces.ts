import {ErrorInterface} from "../../@core/_interfaces/general.interface";
import {ChampionInterfaces} from "./champion.interfaces";


export enum NeedTypes {
  need = "need"
}

export enum NeedStatus {
  draft = "draft",
  listed = "listed",
  claimed = "claimed",
  purchased = "purchased",
  delivered = "delivered",
  pickedUp = "pickedUp"
}

export enum Genders {
  male = "male",
  female = "female",
}

export enum AgeRanges {
  baby = "baby",
  toddler = "toddler",
  preschool = "preschool",
  gradeschool = "gradeschool",
  teen = "teen",
  adult = "adult",
}


export interface NeedsSearchInterfaces {
  keyword?: string;
  county?: string;
  status?: NeedStatus;
  order?: string | null;
  thankYouSent?: number | null;
  limit: number;
  skip?: number;
}

export interface NeedBulkUpdateInterface {
  ids: number[],
  familyName: string;
}

export interface NeedInterface {
  id?: number;
  createdAt?: number;
  updatedAt?: number;
  storageFolder?: string;
  type?: NeedTypes;
  title: string;
  description: string;
  expiresAt?: number;
  price: number;
  imageURL?: string;
  campaignURL?: string;
  donatedValue: number;
  status: NeedStatus;
  socialWorkerName: string;
  socialWorkerPhone: string;
  socialWorkerEmail: string;
  supervisorName: string;
  familyName: string;
  deliveryAddress: string;
  county: string;
  requestType: string;
  itemType: string;
  itemsRequested: string;
  requestedAt?: number;
  gender: Genders[];
  ageRange: AgeRanges[];
  claimedAt?: number;
  itemsOrdered: string;
  deliverDetails: string;
  receiverName: string;
  pickupName: string;
  notes: string;
  championID?: number;
  thankYouSent?: 0 | 1;
  champion?: ChampionInterfaces;
}

export interface ResponseNeeds {
  errorCode: ErrorInterface;
  response?: NeedInterface[];
}

export interface ResponseNeed {
  errorCode: ErrorInterface;
  response?: NeedInterface;
}

