// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  frontURL: 'https://dev.hypersense-software.com/',
  apiURL: 'https://dev.hypersense-software.com/',
  bucket: {
    bucket: 'base-project-development',
    url: 'https://hs-website-api-dev.s3.eu-west-1.amazonaws.com'
  },
  ga_code: 'UA-36129220-10',
  GOOGLE_MAPS: 'AIzaSyABIPIsJ3Y-YJFlIalcFZ674IhiF9QpTlM',
  cdn: 'https://dev.hypersense-software.com',
  RECAPTCHA_key: '6LeUF-kUAAAAAGz47KsLgdK2w7jTekPtZMrZYMcD',
  pagination_limit: '10'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
