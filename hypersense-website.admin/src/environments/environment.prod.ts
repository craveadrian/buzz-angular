export const environment = {
  production: true,
  frontURL: 'https://hypersense-software.com',
  apiURL: 'https://hypersense-software.com',
  bucket: {
    bucket: 'base-project-development',
    url: 'https://hs-website-api-dev.s3.eu-west-1.amazonaws.com'
  },
  ga_code: 'UA-36129220-10',
  GOOGLE_MAPS: 'AIzaSyABIPIsJ3Y-YJFlIalcFZ674IhiF9QpTlM',
  cdn: 'https://dev.hypersense-software.com',
  RECAPTCHA_key: '6LeUF-kUAAAAAGz47KsLgdK2w7jTekPtZMrZYMcD',
  pagination_limit: '10'
};
