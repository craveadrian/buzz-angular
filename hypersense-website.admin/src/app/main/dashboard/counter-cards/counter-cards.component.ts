import { ViewEncapsulation, ChangeDetectionStrategy, Component, Input } from '@angular/core';


@Component({
  selector: 'app-counter-cards',
  templateUrl: './counter-cards.component.html',
  styleUrls: [],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CounterCardsComponent {
  

  @Input() totalPosts = 0;
  @Input() totalProjects = 0;
  @Input() totalTestimonials = 0;

  constructor(
  ) { 
  }

}
