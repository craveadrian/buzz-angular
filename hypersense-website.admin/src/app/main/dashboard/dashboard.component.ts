import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable, Subject, takeUntil } from 'rxjs';
import { GetBlogs, GetBlogsCounter, GetCategories } from 'src/app/shared/store/actions/blog.action';
import { GetCounter, GetProjects } from 'src/app/shared/store/actions/project.action';
import { GetTestimonials, GetTestimonialsCounter } from 'src/app/shared/store/actions/testimonials.action';
import { BlogState } from 'src/app/shared/store/states/blog.state';
import { ProjectState } from 'src/app/shared/store/states/project.state';
import { TestimonialState } from 'src/app/shared/store/states/testimonial.state';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardComponent implements OnInit,OnDestroy {
  title!: string
  postLength: any;
  projectLength: any;
  testimonialLength: any;

  private _unsubscribeAll: Subject<any>;

  totalPosts: any;
  totalProjects: any;
  totalTestimonials: any;

  @Select(ProjectState.counter) projectCounter$!: Observable<any>;
  @Select(BlogState.counter) blogCounter$!: Observable<any>;
  @Select(TestimonialState.counter) testimonialCounter$!: Observable<any>;

  constructor(
    private _store: Store
  ) {
    this._unsubscribeAll = new Subject();
    this.projectLength = localStorage.getItem('projectCounter');
    this.postLength = localStorage.getItem('postCounter');
    this.testimonialLength = localStorage.getItem('testimonialCounter');
  }


  ngOnInit(): void {
    this.title = 'Dashboard';
    this._store.dispatch(new GetBlogsCounter({ status: 1 }));
    this._store.dispatch(new GetCounter({ status: 1 }));
    this._store.dispatch(new GetTestimonialsCounter({}));
    this.totalPosts = this.postLength;
    this.totalProjects = this.projectLength;
    this.totalTestimonials = this.testimonialLength;
    this.projectCounter$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(
        (projectCounter: any) => {
          if (projectCounter){
            this.totalProjects = projectCounter;
          }
        }
      );
    this.blogCounter$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(
        (blogCounter: any) => {
          if (blogCounter) {
            this.totalPosts = blogCounter;
          }
        }
      );

    this.testimonialCounter$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(
        (testimonialCounter: any) => {
          if (testimonialCounter) {
            this.totalTestimonials = testimonialCounter;
          }
        }
      );
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next(true);
    this._unsubscribeAll.complete();
  }

}
