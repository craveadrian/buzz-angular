import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ProjectsFormComponent } from "./projects-form/projects-form.component";
import { ProjectsListComponent } from "./projects-list/projects-list.component";
import { ProjectsComponent } from "./projects.component";

const routes: Routes = [
  {
    path: '',
    component: ProjectsComponent,
    children: [
      {
        path: '',
        component: ProjectsListComponent
      },
      {
        path: 'add',
        component: ProjectsFormComponent
      },
      {
        path: ':id/edit',
        component: ProjectsFormComponent
      }
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
})
export class ProjectsRoutingModule { }