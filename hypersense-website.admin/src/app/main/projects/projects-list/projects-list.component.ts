import { ChangeDetectionStrategy, AfterViewInit, Component, OnDestroy, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { Select, Store } from '@ngxs/store';
import { Observable, Subject } from 'rxjs';
import { takeUntil, debounceTime } from 'rxjs/operators';
import { ProjectState } from 'src/app/shared/store/states/project.state';
import { ChangeOrder, DeleteProject, GetProjects } from 'src/app/shared/store/actions/project.action';
import { environment } from 'src/environments/environment';
import { Project } from 'src/app/shared/models/project.model';
import { ProjectService } from 'src/app/shared/services/project.services';
import Swal from 'sweetalert2';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import * as _ from 'lodash';

@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectsListComponent implements OnInit, AfterViewInit, OnDestroy {
  displayedColumns = ['order','folder', 'title', 'duration', 'name', 'industry', 'slug', 'status', 'technologies', 'updatedAt', 'actions'];
  dataSource = new MatTableDataSource();
  projectList!: Project[];
  projectTable!: Project[];
  pageSize: number = Number(environment.pagination_limit);
  pageSizeOptions = [10, 25, 30];
  pageEvent!: PageEvent;
  isLoading = false;

  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatTable) table!: MatTable<Project>;

  @Select(ProjectState.projects) projects$!: Observable<any>;

  private _unsubscribeAll = new Subject();

  private search = new Subject();

  constructor(
    private _store: Store
  ) { }

  ngOnInit(): void {
    this._store.dispatch(new GetProjects('', 1));
    this.fetchTestmonials();
  }

  ngAfterViewInit(): void {
    this.isLoading = true;
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.search.pipe(
      debounceTime(500)
    ).subscribe(search => {
      this._store.dispatch(new GetProjects(search, 0));
    });
  }

  fetchTestmonials(): void {
    this.projects$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((projects: any) => {
        if (projects) {
          this.projectList = projects;
          this.dataSource = new MatTableDataSource(projects);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.isLoading = false;
        }
      });
  }

  doFilter(filterValue: Event) {
    const filter = (filterValue.target as HTMLInputElement).value.trim().toLowerCase();
    this.search.next(filter);
  }
  onPageChange(event: PageEvent) {
    let page = event.pageIndex;
    page = page * 10;
    this._store.dispatch(new GetProjects('', page));
  }

  onDelete(id: number){
    Swal.fire({
      title: 'Are you sure want to remove?',
      text: 'You will not be able to recover this project!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this._store.dispatch(new DeleteProject(id));
        Swal.fire(
          'Deleted!',
          'Your Project is deleted :)',
          'success'
        )
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'Your Project is safe :)',
          'error'
        )
      }
    })
  }

  drop(event: CdkDragDrop<any>) {
    const previousIndex = event.previousIndex;
    const order: any = event.currentIndex + 1
    const indexId: any = Object.assign({}, this.dataSource.data[previousIndex]);
    const data = Object.assign({}, { order: order.toString(), id: indexId.id.toString() });
    this._store.dispatch(new ChangeOrder(data)).subscribe(
      (data: any) => {
        this.dataSource.data = data.project.projects;
      }
    );
    this.table.renderRows();
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next(null);
    this._unsubscribeAll.complete();
  }
}
