import { ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styles: [
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectsComponent implements OnInit {
  title!: string
  constructor(

  ) { }

  ngOnInit(): void {
   this.title = "Projects"
  }

}

