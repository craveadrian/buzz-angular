import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Editor, Toolbar } from 'ngx-editor';
import { Observable, Subject, takeUntil } from 'rxjs';
import { Project, ProjectDefaults } from 'src/app/shared/models/project.model';
import { User } from 'src/app/shared/models/user.model';
import { AwsStorageService } from 'src/app/shared/services/aws.storage.service';
import { LogoutAuth } from 'src/app/shared/store/actions/auth.actions';
import { GetProject, NewProject, SaveProject } from 'src/app/shared/store/actions/project.action';
import { AuthState } from 'src/app/shared/store/states/auth.state';
import { ProjectState } from 'src/app/shared/store/states/project.state';
import UploadImageAdapter from 'src/app/shared/upload-image/upload-image-adapter';
import { environment } from 'src/environments/environment';
import * as ClassicEditor from 'ckeditor5-build-classic-simple-upload-adapter-image-resize';

@Component({
  selector: 'app-projects-form',
  templateUrl: './projects-form.component.html',
  styleUrls: []
})
export class ProjectsFormComponent implements OnInit, OnDestroy {
  auth!: User;
  projectData?: Project;
  baseURL = environment.cdn; 
  folder: any;
  file!: any;
  bucketImage = 'portfolios-assets';

  public contentEditor = ClassicEditor;
  public contentRightEditor = ClassicEditor;
  
  projectForm!: FormGroup;

  technologyList: string[] = [
    'Android', 'iOS', 'Angular', 'Node.js', 
    'Wordpress', 'Wearables','PHP','Flutter'];
  industryList: string[] = [
    'Business Intelligence', 'Healthcate & Fitness',
    'Fintech', 'VoIP', 'Enterprice','Health Care',
    'Automitive', 'Lifestyle','Ecommerce', 'Games',
    'Image Processing', 'Fitness','AR'
  ]

  private _unsubscribeAll = new Subject();

  @Select(ProjectState.current) project$!: Observable<Project>;

  constructor(
    private _formBuilder: FormBuilder,
    private _store: Store,
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _awsService: AwsStorageService
  ) { }

  ngOnInit(): void {
    this.auth = JSON.parse(localStorage.getItem('currentUser') || '{}');
    this.myForm();
    this._store.dispatch(new NewProject());
    this._activatedRoute.params
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(
        (params: Params) => {
          const id = params['id'];

          if (id && id !== 'undefined') {
            this._store.dispatch(new GetProject(id));
          }
        }
      );

    this.project$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((project) => {
        let technologies = [];
        this.projectData = project;
        if (project.technologies) {
          technologies = JSON.parse(project.technologies);
        }

        if (project.folder) {
          this.folder = project.folder;
        } else if (this.auth.storageFolder) {
          this.folder = this.auth.storageFolder;
        } else {
          this._store.dispatch(new LogoutAuth());
        }
        this.projectForm.patchValue({ 
            ...project, 
            technologies: technologies, 
            folder: this.folder
          });
      });
  }

  myForm() {
    this.projectForm = this._formBuilder.group({
      id: [ProjectDefaults.id],
      project: [ProjectDefaults.project],
      slug: [ProjectDefaults.slug],
      title: [ProjectDefaults.title],
      content: [ProjectDefaults.content],
      contentRight: [ProjectDefaults.contentRight],
      excerpt: [ProjectDefaults.excerpt],
      industry: [ProjectDefaults.industry],
      technologies: [ProjectDefaults.technologies],
      status: [ProjectDefaults.status],
      homepage: [ProjectDefaults.homepage],
      caseStudy: [ProjectDefaults.caseStudy],
      software: [ProjectDefaults.software],
      node: [ProjectDefaults.node],
      imageURL: [ProjectDefaults.imageURL],
      folder: [ProjectDefaults.folder],
      seoMetatitle: [ProjectDefaults.seoMetatitle],
      seoDescription: [ProjectDefaults.seoMetatitle],
      seoKeywords: [ProjectDefaults.seoKeywords],
      seoImageURL: [ProjectDefaults.seoImageURL],
    })
  }

  onSubmit() {
    if(this.projectForm.valid){
      const testimonialFormValues= Object.assign({}, this.projectForm.value);
      
      this._store.dispatch(new SaveProject(testimonialFormValues));
    }
  }

  addImageURL(event: any){
    const file = event;
    const imageURL = this.bucketImage + '/' + this.folder + '/' + event.name;
    this._awsService.getAwsLinkAndUpload(imageURL, file, "awsStorages")
      .then(
        data => {
          this.projectForm.patchValue({ imageURL: data });
        }
      );
  }

  addSeoImageUrl(event: any){
    const file = event;
    const seoImageURL = this.bucketImage + '/' + this.folder + '/' + event.name;
    this._awsService.getAwsLinkAndUpload(seoImageURL, file, "awsStorages")
      .then(
        data => {

          this.projectForm.patchValue({ seoImageURL: data });
        }
      );
    
  }

  onReady(editor: any): void {
    if (editor.model.schema.isRegistered('image')) {
      editor.model.schema.extend('image', { allowAttributes: 'blockIndent' })
    }
    editor.plugins.get('FileRepository').createUploadAdapter = (loader: any) => {
      return new UploadImageAdapter(loader, editor.config, this._awsService, this.bucketImage + '/' + this.folder);
    }
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next(null);
    this._unsubscribeAll.complete();
    this.projectForm.reset();
  }

}
