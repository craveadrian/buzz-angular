import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Route } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Subject, Observable } from 'rxjs';
import { Validators } from 'ngx-editor';
import { takeUntil } from 'rxjs/operators';
import { CreateTestimonial, GetTestimonial, NewTestimonial } from 'src/app/shared/store/actions/testimonials.action';
import { Testimonial, TestimonialDefaults } from 'src/app/shared/models/testimonial.model';
import { TestimonialState } from 'src/app/shared/store/states/testimonial.state';
import { AwsStorageService } from 'src/app/shared/services/aws.storage.service';
import { AuthState } from 'src/app/shared/store/states/auth.state';
import { User } from 'src/app/shared/models/user.model';
import { environment } from 'src/environments/environment';
import { LogoutAuth } from 'src/app/shared/store/actions/auth.actions';

@Component({
  selector: 'app-testimonials-form',
  templateUrl: './testimonials-form.component.html',
  styleUrls: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestimonialsFormComponent implements OnInit, OnDestroy {
  auth!: User | any;
  file!: any;
  imageURL: any;
  base = 'testimonials';
  srcURL: any;
  bucketImage = 'testimonials-assets';
  testimonials! : Testimonial;
  folder!: any;

  testimonialForm!: FormGroup;

  private _unsubscribeAll = new Subject();

  @Select(TestimonialState.current) testimonial$!: Observable<any>;

  constructor(
    private _formBuilder: FormBuilder,
    private _store: Store,
    private _activatedRoute: ActivatedRoute,
    private _awsService: AwsStorageService
  ) { }

  ngOnInit(): void {
    this.auth = JSON.parse(localStorage.getItem('currentUser') || '{}');
    this.myForm();
    this._store.dispatch(new NewTestimonial());
    this._activatedRoute.params
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(
        (params: Params) => {
          const id = params['id'];

          if (id && id !== 'undefined') {
            this._store.dispatch(new GetTestimonial(id));
          }
        }
      );
    this.testimonial$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((testimonial) => {
        if(testimonial.folder){
          this.folder = testimonial.folder;
        } else if (this.auth.storageFolder){
          this.folder = this.auth.storageFolder;
        } else {
          this._store.dispatch(new LogoutAuth());
        }
        this.testimonialForm
          .patchValue({ ...testimonial, folder: this.folder});
        this.testimonials = testimonial;
        this.srcURL = environment.cdn + '/' + testimonial.src;
      });
  }

  myForm(): void {
    this.testimonialForm = this._formBuilder.group({
      id: [TestimonialDefaults.id],
      sourceURL: [TestimonialDefaults.sourceURL],
      source: [TestimonialDefaults.source],
      name: [TestimonialDefaults.name, [Validators.required]],
      location: [TestimonialDefaults.location],
      title: [TestimonialDefaults.title],
      company: [TestimonialDefaults.company],
      description: [TestimonialDefaults.description],
      rank: [TestimonialDefaults.rank],
      src: [TestimonialDefaults.src],
      folder: [TestimonialDefaults.folder]
    });

  }


  onSubmit() {
    if(this.testimonialForm.valid){
      const testimonialFormValues = Object.assign({}, this.testimonialForm.value);

      this._store.dispatch(new CreateTestimonial(testimonialFormValues));
      
    }
  }

  addImageURL(event: any) {
    this.file = event;
    const imageURL = this.bucketImage + '/' + this.folder + '/' + event.name;
    const link = this._awsService.getAwsLinkAndUpload(imageURL, this.file, "awsStorages");
    this.testimonialForm.patchValue(
      { src: imageURL }
    )
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next(null);
    this._unsubscribeAll.complete();
  }
}
