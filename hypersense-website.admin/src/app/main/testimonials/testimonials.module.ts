import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestimonialsComponent } from './testimonials.component';
import { TestimonialsRoutingModule } from './testimonials-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { TestimonialsListComponent } from './testimonials-list/testimonials-list.component';
import { TestimonialsFormComponent } from './testimonials-form/testimonials-form.component';

@NgModule({
  declarations: [
    TestimonialsComponent,
    TestimonialsListComponent,
    TestimonialsFormComponent,
  ],
  imports: [
    CommonModule,
    TestimonialsRoutingModule,
    SharedModule
  ]
})
export class TestimonialsModule { }
