import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TestimonialsFormComponent } from "./testimonials-form/testimonials-form.component";
import { TestimonialsListComponent } from "./testimonials-list/testimonials-list.component";
import { TestimonialsComponent } from "./testimonials.component";

const routes: Routes = [
  {
    path: '',
    component: TestimonialsComponent,
    children: [
      {
        path: '',
        component: TestimonialsListComponent
      },
      {
        path: 'add',
        component: TestimonialsFormComponent
      },
      {
        path: ':id/edit',
        component: TestimonialsFormComponent
      }
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
})
export class TestimonialsRoutingModule { }