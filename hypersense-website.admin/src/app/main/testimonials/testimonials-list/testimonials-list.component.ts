import { AfterViewInit, Component, OnInit, ViewChild, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable, Subject } from 'rxjs';
import { takeUntil, debounceTime } from 'rxjs/operators';
import { DeleteAuth } from 'src/app/shared/store/actions/auth.actions';
import { DeleteTestimonial, GetTestimonials } from 'src/app/shared/store/actions/testimonials.action';
import { Testimonial } from 'src/app/shared/models/testimonial.model';
import { TestimonialsService } from 'src/app/shared/services/testimonials.service';
import { TestimonialState } from 'src/app/shared/store/states/testimonial.state';
import { environment } from 'src/environments/environment';
import  Swal  from 'sweetalert2';

@Component({
  selector: 'app-testimonials-list',
  templateUrl: './testimonials-list.component.html',
  styleUrls: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestimonialsListComponent implements OnInit, AfterViewInit, OnDestroy {
  displayedColumns = ['source', 'name', 'location', 'title', 'company', 'rank', 'updatedAt', 'actions'];
  dataSource = new MatTableDataSource<Testimonial>();
  testimonialList!: Testimonial[];
  testimonialTable!: Testimonial[];
  pageSize: number = Number(environment.pagination_limit);
  pageSizeOptions = [10, 25, 30];
  pageEvent!: PageEvent;
  isLoading = false;

  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  @Select(TestimonialState.testimonials) testimonials$!: Observable<any>;

  private _unsubscribeAll = new Subject();

  private search = new Subject();

  constructor(
    private _store: Store,
    private router: Router
  ) { }

  ngOnInit(): void {
    this._store.dispatch(new GetTestimonials('', 1));
    this.fetchTestmonials();
  }

  ngAfterViewInit(): void {
    this.isLoading = true;
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.search.pipe(
      debounceTime(500)
    ).subscribe(search => {
      this._store.dispatch(new GetTestimonials(search, 0));
    });
  }

  fetchTestmonials(): void {
    this.testimonials$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((testimonials: any) => {
        if (testimonials) {
          this.testimonialList = testimonials;
          this.dataSource = new MatTableDataSource(testimonials);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.isLoading = false;
        }
      });
  }

  doFilter(filterValue: Event) {
    const filter = (filterValue.target as HTMLInputElement).value.trim().toLowerCase();
    this.search.next(filter);
  }

  onPageChange(event: PageEvent){
    let page = event.pageIndex;
    page = page * 10;
    this._store.dispatch(new GetTestimonials('', page));
  }

  onDelete(id: number){
    Swal.fire({
      title: 'Are you sure want to remove?',
      text: 'You will not be able to recover this testimonial!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this._store.dispatch(new DeleteTestimonial(id));
        Swal.fire(
          'Deleted!',
          'Your Testimonial is deleted :)',
          'success'
        )
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'Your Testimonial is safe :)',
          'error'
        )
      }
    })
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next(true);
    this._unsubscribeAll.complete();
  }

}

