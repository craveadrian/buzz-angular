import { AfterViewInit, Component, OnInit, ViewChild, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Select, Store } from '@ngxs/store';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DeleteBlog, GetBlogs } from 'src/app/shared/store/actions/blog.action';
import { GlobalService } from 'src/app/shared/services/global.service';
import { BlogState } from 'src/app/shared/store/states/blog.state';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BlogListComponent implements OnInit, AfterViewInit, OnDestroy {

  displayedColumns = ['folder', 'title', 'slug', 'status', 'publishURLPath', 'updatedAt', 'actions'];
  dataSource = new MatTableDataSource<any>();
  blogList!: any[];
  blogTable!: any[];
  pageLimit: number = Number(environment.pagination_limit);

  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  @Select(BlogState.blogs) blogs$!: Observable<any>;

  private _unsubscribeAll = new Subject();

  constructor(
    private _store: Store,
    private _globalService: GlobalService,
  ) { }

  ngOnInit(): void {
    this._store.dispatch(new GetBlogs(''));
    this.fetchBlogs();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  fetchBlogs(): void {
    this.blogs$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((blogs: any) => {
        if (blogs) {
          this.blogList = blogs;
          this.dataSource = new MatTableDataSource(blogs);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      });
  }

  doFilter(filterValue: Event) {
    this.dataSource.filter = (filterValue.target as HTMLInputElement).value.trim().toLowerCase();
  }

  onDelete(id: number) {
    Swal.fire({
      title: 'Are you sure want to remove?',
      text: 'You will not be able to recover this post!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {
        this._store.dispatch(new DeleteBlog(id));
        Swal.fire(
          'Deleted!',
          'Your Post is deleted :)',
          'success'
        )
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelled',
          'Your Post is safe :)',
          'error'
        )
      }
    })
  }

  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next(null);
    this._unsubscribeAll.complete();
  }

}

