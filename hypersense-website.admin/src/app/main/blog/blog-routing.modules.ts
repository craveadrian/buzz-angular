import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { BlogFormComponent } from "./blog-form/blog-form.component";
import { BlogListComponent } from "./blog-list/blog-list.component";
import { BlogComponent } from "./blog.component";

const routes: Routes = [
  {
    path: '',
    component: BlogComponent,
    children: [
      {
        path: '',
        component: BlogListComponent
      },
      {
        path: 'add',
        component: BlogFormComponent
      },
      {
        path: ':id/edit',
        component: BlogFormComponent
      }
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
})
export class BlogRoutingModule { }