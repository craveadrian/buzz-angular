import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Editor, Toolbar, Validators } from 'ngx-editor';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Blog, BlogDefaults } from 'src/app/shared/models/blog.model';
import { AwsStorageService } from 'src/app/shared/services/aws.storage.service';
import { LogoutAuth } from 'src/app/shared/store/actions/auth.actions';
import { GetBlog, GetCategories, NewBlog, SaveBlog } from 'src/app/shared/store/actions/blog.action';
import { BlogState } from 'src/app/shared/store/states/blog.state';
import { environment } from 'src/environments/environment';
import * as ClassicEditor from 'ckeditor5-build-classic-simple-upload-adapter-image-resize';
import { HttpClient } from '@angular/common/http';
import { RequestService } from 'src/app/shared/services/request.service';
import UploadImageAdapter from 'src/app/shared/upload-image/upload-image-adapter';

@Component({
  selector: 'app-blog-form',
  templateUrl: './blog-form.component.html',
  styleUrls: []
})
export class BlogFormComponent implements OnInit, OnDestroy {
  auth!: any;
  folder: any;
  bucketImage = 'blogs-assets';
  blogForm! : FormGroup;
  posts: any;
  categoryList: any[] = [];
  baseURL = environment.bucket.url;
  config: any;
  public contentEditor = ClassicEditor;
  private _unsubscribeAll = new Subject();

  @Select(BlogState.current) blog$!: Observable<Blog>;
  @Select(BlogState.categories) categories$!: Observable<any>;

  constructor(
    private _formBuilder: FormBuilder,
    private _store: Store,
    private _activatedRoute: ActivatedRoute,
    private _awsStorage: AwsStorageService,
    private http: HttpClient,
    private _requestService: RequestService
  ) { }

  ngOnInit(): void {
    this.auth = JSON.parse(localStorage.getItem('currentUser') || '{}');
    this.myForm();
    this._store.dispatch(new NewBlog());

    this._activatedRoute.params
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(
        (params: Params) => {
          const id = params['id'];

          if (id && id !== 'undefined') {
            this._store.dispatch(new GetBlog(id));
          }
        }
      );

    this._store.dispatch(new GetCategories());
    this.categories$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(
        (categories: any) => {
          this.categoryList = categories;
        }
      )
    
    this.blog$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((blog: any) => {
        const results = [];
        for(let key in blog.categories){
          results?.push(blog.categories[key].id)
        }
        this.posts = blog;
        if (blog.folder) {
          this.folder = blog.folder;
        } else if (this.auth.storageFolder) {
          this.folder = this.auth.storageFolder;
        } else {
          this._store.dispatch(new LogoutAuth());
        }
        this.blogForm.patchValue({
          ...blog,
          categories: results
        })
      });

  }

  myForm(): void {
    this.blogForm = this._formBuilder.group({
      id: [BlogDefaults.id],
      title: [BlogDefaults.title, [Validators.required]],
      slug: [BlogDefaults.slug],
      content: [BlogDefaults.content],
      excerpt: [BlogDefaults.excerpt],
      status: [BlogDefaults.status],
      imageURL: [BlogDefaults.imageURL],
      imageURLCard: [BlogDefaults.imageURL],
      categories: [BlogDefaults.categories],
      seoMetatitle: [BlogDefaults.seoMetatitle],
      seoDescription: [BlogDefaults.seoMetatitle],
      seoKeywords: [BlogDefaults.seoKeywords],
      seoImageUrl: [BlogDefaults.seoImageURL],
      folder: [BlogDefaults.folder, [Validators.required]]
    });

  }

  addImageURL(event: any) {
    const file = event;
    const imageURL = this.bucketImage + '/' + this.folder + '/' + event.name;
    this._awsStorage.getAwsLinkAndUpload(imageURL, file, "awsStorages")
      .then(
        data => {
          this.blogForm.patchValue({ imageURL: data });
        }
      );
  }

  addSeoImageUrl(event: any) {
    const file = event;
    const seoImageURL = this.bucketImage + '/' + this.folder + '/' + event.name;
    this._awsStorage.getAwsLinkAndUpload(seoImageURL, file, "awsStorages")
      .then(
        data => {
          this.blogForm.patchValue({ seoImageURL: data });
        }
      );

  }
  addImageURLCard(event: any){
    const file = event;
    const imageURLCard = this.bucketImage + '/' + this.folder + '/' + event.name;
    this._awsStorage.getAwsLinkAndUpload(imageURLCard, file, "awsStorages")
      .then(
        data => {
          this.blogForm.patchValue({ imageURLCard: data });
        }
      );
  }
  onSubmit(): void {
    if (this.blogForm.valid) {
      const blogFormValues = Object.assign({}, this.blogForm.value);
      this._store.dispatch(new SaveBlog(blogFormValues));

    }
  }

  onReady(editor: any): void {
    if (editor.model.schema.isRegistered('image')) {
      editor.model.schema.extend('image', {allowAttributes: 'blockIndent'})
    }
    editor.plugins.get('FileRepository').createUploadAdapter = (loader:any) => {
      return new UploadImageAdapter(loader, editor.config, this._awsStorage, this.bucketImage + '/' + this.folder);
    }
  }
  ngOnDestroy(): void {
    this._unsubscribeAll.next(null);
    this._unsubscribeAll.complete();
  }

}
