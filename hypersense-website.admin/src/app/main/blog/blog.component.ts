import { ChangeDetectionStrategy, Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styles: [
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BlogComponent implements OnInit {
  title!: string;
  constructor(

  ) { }

  ngOnInit(): void {
    this.title = "Posts"
  }
}
