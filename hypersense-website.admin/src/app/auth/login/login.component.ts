import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { LoginAuth } from 'src/app/shared/store/actions/auth.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit {
  public loginForm!: FormGroup;
  private isSubmitted = false;
  hide = true;
  constructor(
    private _formBuilder: FormBuilder,
    private _store: Store
  ) { }

  ngOnInit(): void {
    this.loginFormGroup();
  }

  loginFormGroup(): void {
    this.loginForm = 
    this._formBuilder.group({
      email: ["", [Validators.email, Validators.required]],
      password: [
        "",
        [
          Validators.required,
        ]
      ]
    });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.loginForm.valid) {
      const loginFormValues = Object.assign({}, this.loginForm.value);
      this._store.dispatch(new LoginAuth(loginFormValues));
    }
  }

}
