import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { RequestService } from './request.service';

@Injectable({ providedIn: 'root' })
export class AwsStorageService {
  constructor(
    private http: HttpClient,
    private _requestService : RequestService
  ) {
  }
  
  getCustomSignedUrl(type: any, key: any, prefix: any) {
    const params : any = { type, key };
    return this._requestService.get('api/v1/' + prefix + '/generateTempSignedPutURL', params);
  }

  uploadFileAWSS3(fileuploadurl: any, file: any){
    return new Promise<any>((resolve, reject) => {
      const httpOptions = {
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Content-Type': file.type,
          Accept: 'application/json, text/plain, */*'
        }
      };
      this.http.put(fileuploadurl, file, httpOptions).toPromise()
        .then(
          res =>  // Success
            resolve(res)
          ,
          msg =>  // Error
            reject(msg)

        );
    });
  }

  getAwsLinkAndUpload(key: any, file: File, prefix: any) {
    return new Promise<string>((resolve, reject) => {
      // get signed url
      this.getCustomSignedUrl(file.type, key, prefix).subscribe(
        (data: any) => {
          if (data.response) {
            if (data.response.exists) {
              resolve(key);
            } else {
              this.uploadFileAWSS3(data.response.url, file).then(
                response => {
                  resolve(key);
                }).catch(err => {
                  reject(err);
                });
            }
          } else {
            reject('Error');
          }
        },
        (err) => {
          reject(err);
        }
      );
    });


  }

}
