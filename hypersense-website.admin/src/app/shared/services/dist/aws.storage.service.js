"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AwsStorageService = void 0;
var core_1 = require("@angular/core");
var AwsStorageService = /** @class */ (function () {
    function AwsStorageService(http, _requestService) {
        this.http = http;
        this._requestService = _requestService;
    }
    AwsStorageService.prototype.getCustomSignedUrl = function (type, key, prefix) {
        var params = { type: type, key: key };
        return this._requestService.get('api/v1/' + prefix + '/generateTempSignedPutURL', params);
    };
    AwsStorageService.prototype.uploadFileAWSS3 = function (fileuploadurl, file) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var httpOptions = {
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': file.type,
                    Accept: 'application/json, text/plain, */*'
                }
            };
            _this.http.put(fileuploadurl, file, httpOptions).toPromise()
                .then(function (res) {
                return resolve(res);
            }, function (msg) {
                return reject(msg);
            });
        });
    };
    AwsStorageService.prototype.getAwsLinkAndUpload = function (key, file, prefix) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // get signed url
            _this.getCustomSignedUrl(file.type, key, prefix).subscribe(function (data) {
                if (data.response) {
                    if (data.response.exists) {
                        resolve(key);
                    }
                    else {
                        _this.uploadFileAWSS3(data.response.url, file).then(function (response) {
                            resolve(key);
                        })["catch"](function (err) {
                            reject(err);
                        });
                    }
                }
                else {
                    reject('Error');
                }
            }, function (err) {
                reject(err);
            });
        });
    };
    AwsStorageService = __decorate([
        core_1.Injectable({ providedIn: 'root' })
    ], AwsStorageService);
    return AwsStorageService;
}());
exports.AwsStorageService = AwsStorageService;
