import { Injectable, Optional } from '@angular/core';
import { 
  HttpErrorResponse,
  HttpEvent, 
  HttpHandler, 
  HttpInterceptor, 
  HttpRequest } from '@angular/common/http';
import { Observable, tap } from 'rxjs';
import { Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { REQUEST } from '@nguniversal/express-engine/tokens';
import { AuthService } from '../services/auth.service';
import { Store } from '@ngxs/store';
import { DeleteAuth, LoginAuth, LogoutAuth } from '../store/actions/auth.actions';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    @Optional() @Inject(REQUEST) protected request: any,
    @Inject(PLATFORM_ID) private platformId: any,
    public auth: AuthService,
    private _store: Store,
    private _router: Router
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let serverReq: HttpRequest<any> = request;
    if (request) {
      if (isPlatformBrowser(this.platformId) &&
        this.auth.getToken(request, this.platformId)
      ) {
        if (request.url.indexOf('.s3.') === -1) {
          Object.assign(request, {
            params: request.params.append('access_token', this.auth.getToken(request, this.platformId)),
            setHeaders: {
              Authorization: this.auth.getToken(request, this.platformId)
            }
          });
        }
      }
      serverReq = request.clone(request);
    }

    return next.handle(serverReq).pipe( tap(()=> {},
      (err: any) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status !== 401){
            return;
          }
          this.auth.logout();
          this._store.dispatch(new DeleteAuth());
          this._router.navigate(['/auth/login']);
        }
      }
    ));
  }
}

