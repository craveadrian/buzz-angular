import { environment } from "src/environments/environment";
import { AwsStorageService } from "../services/aws.storage.service";

export default class UploadImageAdapter {
  loader;
  reader: any;
  config;
  aws!:AwsStorageService;
  folder: any;
  imageURL: any;
  constructor(loader: any, config: any, aws: any, folder: any) {
    this.loader = loader;
    this.config = config;
    this.aws = aws;
    this.folder = folder;
  }


  public async upload(): Promise<any> {
    const value = await this.loader.file;
    const key = this.folder + '/' + value.name
    return this.read(value);
    
    
  }

  read(file: any) {
    const key = this.folder + '/' + file.name;
    
    return new Promise((resolve, reject) => {
      this.aws.getAwsLinkAndUpload(key, file, "awsStorages").then(
        data => {
          const dataURL = environment.bucket.url + '/' + data;
          const reader = new FileReader();
          resolve({ default: dataURL });

          reader.onerror = function (error) {
            reject(error);
          };

          reader.onabort = function () {
            reject();
          };
          reader.readAsDataURL(file);
        }
      );
      
    });
  }

  abort() {
    if (this.reader) {
      this.reader.abort();
    }
  }
}