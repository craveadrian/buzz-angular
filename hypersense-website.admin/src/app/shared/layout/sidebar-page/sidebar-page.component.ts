import { ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'app-sidebar-page',
  templateUrl: './sidebar-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidebarPageComponent {

  constructor() { }

}
