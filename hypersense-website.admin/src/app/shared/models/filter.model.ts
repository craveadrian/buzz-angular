export interface Filter {
  keyword? : string;
  limit? : number;
  skip? : number;
}